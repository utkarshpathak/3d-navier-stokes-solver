#include "mpi.h"
#include "helper.hpp"
#include "visual.hpp"
#include "init.hpp"
#include "sor.hpp"
#include <iostream>
#include "boundary_val.hpp"
#include "uvp.hpp"
#include "list"
#include "parallel.h"
#include "algorithm"

double Re;               /* reynolds number   */
double UI;                /* velocity x-direction */
double VI;                /* velocity y-direction */
double PI;                /* pressure */
double GX;                /* gravitation x-direction */
double GY;               /* gravitation y-direction */
double t_end;             /* end time */
double xlength;           /* length of the domain x-dir.*/
double ylength;           /* length of the domain y-dir.*/
double dt;                /* time step */
double dx;                /* length of a cell x-dir. */
double dy;                /* length of a cell y-dir. */
int imax;                /* number of cells x-direction*/
int jmax;                /* number of cells y-direction*/
double alpha;             /* uppwind differencing factor*/
double omg;               /* relaxation factor */
double tau;               /* safety factor for time step*/
int itermax;             /* max. number of iterations for pressure per time step */
double eps;               /* accuracy bound for pressure*/
double dt_value;          /* time for output */
std::string Problem;
std::string geomFile;
int **geometry;
double inflowU;
double inflowV;
double T_h;
double T_c;

double TI;
double PR;
double beta;

int iproc;
int jproc;

std::list<Cell *> slipCells;
std::list<Cell *> inFlowCells;
std::list<Cell *> outFlowCells;
std::list<Cell *> fluidCells;

std::string inputFile = "../InputFiles/LidDrivenCavity.dat";
std::string outFileName = "../outputfiles/LidDrivenCavityParallel";
bool dynamicTimestepping = 1;

double **initDoublePointerToZero(int rows, int columns) {
    std::cout << "Inside initDpointer: " << rows << ", " << columns << "\n";
    double **out;
    out = new double *[rows];
    for (int i = 0; i < rows; i++) {
        out[i] = new double[columns];
        for (int j = 0; j < columns; j++) {
            out[i][j] = 0;
        }
    }
    return out;
}

void destroyDoublePointer(double **P, int rows) {
    for (int i = 0; i < rows; i++) {
        delete P[i];
    }
    delete P;
}


int **createBox(int imax, int jmax) {
    int **out;
    out = new int *[imax + 2];
    for (int i = 0; i <= imax + 1; i++) {
        out[i] = new int[jmax + 2];
        for (int j = 0; j <= jmax + 1; j++) {
            out[i][j] = 0;
            if (i == 0 || i == imax + 1 || j == 0 || j == jmax + 1) {
                out[i][j] = 0; // NoSlip
            } else {
                out[i][j] = 4; // Fluid
            }
        }
    }
    return out;

}

double **toPointer(matrix<double> &M) {
    double **out;
    out = new double *[imax + 2];
    for (int i = 0; i <= imax + 1; i++) {
        out[i] = new double[jmax + 2];
        for (int j = 0; j <= jmax + 1; j++) {
            out[i][j] = M.at(i).at(j);
        }
    }
    return out;
}

int **toPointer(matrix<int> &M, int a, int b) {
    int **out;
    out = new int *[a];
    for (int i = 0; i < a; i++) {
        out[i] = new int[b];
        for (int j = 0; j < b; j++) {
            out[i][j] = M.at(i).at(j);
        }
    }
    return out;
}

double **toPointer(matrix<double> &M, int a, int b) {
    assert(M.size() == a);
    assert(M[0].size() == b);
    double **out;
    out = new double *[a];
    for (int i = 0; i < a; i++) {
        out[i] = new double[b];
        for (int j = 0; j < b; j++) {
            out[i][j] = M.at(i).at(j);
        }
    }
    return out;
}

void destroyPointer(double **P, int imax, int bordersize = 1) {
    for (int i = 0; i <= imax + bordersize; i++) {
        delete P[i];
    }
    delete P;
}

void toMatrix(double **M, matrix<double> &P, int a, int b) {
    P.resize(a, std::vector(b, 0.0));
    for (int i = 0; i < a; i++) {
        for (int j = 0; j < b; j++) {
            P[i][j] = M[i][j];
        }
    }

}

void createOutputFile(Grid &grid, std::string outFileName, int n, int **geo, int imax, int jmax) {
    matrix<double> U, V, P, T;
    grid.velocity(U, velocity_type::U);
    grid.velocity(V, velocity_type::V);
    grid.pressure(P);
    grid.temperature(T);

    double **Up;
    double **Vp;
    double **Pp;
    double **Tp;
    Up = toPointer(U);
    Vp = toPointer(V);
    Pp = toPointer(P);
    Tp = toPointer(T);
    write_vtkFile(outFileName.c_str(), n, xlength, ylength, imax, jmax, dx, dy, Up, Vp, Pp, geo, Tp);

    destroyPointer(Up, imax);
    destroyPointer(Vp, imax);
    destroyPointer(Pp, imax);
    destroyPointer(Tp, imax);
}

void
createOutputFileParallel(double **u, double **v, double **p, std::string outFileName, int n, int **geo, int imax,
                         int jmax, int il, int jb) {
    //pass u for temperature, because Temperature doesnt matter anyway
    write_vtkFileSubdomain(outFileName.c_str(), n, xlength, ylength, imax, jmax, dx, dy, u, v, p, geo, il, jb);
}

void fillList(Grid &grid, int **geo, std::list<Cell *> &noSlipList, std::list<Cell *> &inFlowList,
              std::list<Cell *> &outFlowList, std::list<Cell *> &fluidList, int imax, int jmax) {
    //geo == 0 :: no slip condition , geo == 1 :: free slip condition
    for (int i = 0; i <= imax + 1; i++) {
        for (int j = 0; j <= jmax + 1; j++) {
            assert(grid.cell(i, j).getCellType() == static_cast<cell_type>(geo[i][j]));
            if (geo[i][j] == 0 || geo[i][j] == 1) {
                noSlipList.push_back(&grid.cell(i, j));
            }
            if (geo[i][j] == 2) {
                outFlowList.push_back(&grid.cell(i, j));
            }
            if (geo[i][j] == 3) {
                inFlowList.push_back(&grid.cell(i, j));
            }
            if (geo[i][j] == 4) {
                fluidList.push_back(&grid.cell(i, j));
            }
        }
    }
}

void fillListSubdomain(Grid &grid, int **geo, std::list<Cell *> &noSlipList, std::list<Cell *> &inFlowList,
                       std::list<Cell *> &outFlowList, std::list<Cell *> &fluidList, int imax, int jmax) {
    //geo == 0 :: no slip condition , geo == 1 :: free slip condition
    for (int i = 1; i <= imax + 1; i++) {
        for (int j = 1; j <= jmax + 1; j++) {
            if (geo[i][j] == 0 || geo[i][j] == 1) {
                noSlipList.push_back(&grid.cell(i, j));
            }
            if (geo[i][j] == 2) {
                outFlowList.push_back(&grid.cell(i, j));
            }
            if (geo[i][j] == 3) {
                inFlowList.push_back(&grid.cell(i, j));
            }
            if (geo[i][j] == 4) {
                if (i != 1 && i != imax + 1 && j != 1 && j != jmax + 1) { // only add Fluid cells in the actual Domain
                    fluidList.push_back(&grid.cell(i, j));
                }
            }
        }
    }
}


void checkGeometry(std::list<Cell *> &cells) {
    for (Cell *c : cells) {

        if (c->getEast()->getCellType() == cell_type::FLUID && c->getWest()->getCellType() == cell_type::FLUID) {
            ERROR("Geometry requirements not met at East and West direction");
        }

        if (c->getNorth()->getCellType() == cell_type::FLUID && c->getSouth()->getCellType() == cell_type::FLUID) {
            ERROR("Geometry requirements not met at North South direction");
        }
    }
}


void exchangeAndCalcRes(int rank, double &LocalRes, double &Overallres) {
    double Res[iproc * jproc];
    MPI_Gather(&LocalRes, 1, MPI_DOUBLE, &Res, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    if (rank == 0) {
        Overallres = 0;
        for (int i = 0; i < iproc * jproc; i++) {
            Overallres += Res[i];
        }
        Overallres /= iproc * jproc;
    }

    MPI_Bcast(&Overallres, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
}

//bool found = (std::find(my_list.begin(), my_list.end(), my_var) != my_list.end());

void TestLists(int imax, int jmax, Grid &subdomain) {
    for (int i = 1; i <= imax + 1; i++) {
        for (int j = 1; j <= jmax + 1; j++) {
            cell_type A = subdomain.cell(i, j).getCellType();
            if ((int) A == 0 || (int) A == 1) {
                assert(std::find(slipCells.begin(), slipCells.end(), &subdomain.cell(i, j)) != slipCells.end());
            } else if ((int) A == 2) {
                assert(std::find(outFlowCells.begin(), outFlowCells.end(), &subdomain.cell(i, j)) !=
                       outFlowCells.end());
            } else if ((int) A == 3) {
                assert(std::find(inFlowCells.begin(), inFlowCells.end(), &subdomain.cell(i, j)) != inFlowCells.end());
            } else {
                if (i != 1 && i != imax + 1 && j != 1 && j != jmax + 1) {
                    assert(std::find(fluidCells.begin(), fluidCells.end(), &subdomain.cell(i, j)) != fluidCells.end());
                }
            }
        }
    }
}


void setBoundaries(std::string Problem, int imax, int jmax, Grid &grid, std::list<Cell *> &slipCells, double inflowU,
                   double inflowV, double T_h, double T_c) {
    if (Problem == "LidDrivenCavity") {
        LidDrivenCavity(imax, jmax, grid, inflowU, inflowV);
    } else if (Problem == "PlaneShearFlow") {
        PlaneShearFlow(imax, jmax, grid, slipCells, inflowU, inflowV);
    } else if (Problem == "KarmanVortexStreet") {
        KarmanVortexStreet(imax, jmax, grid, slipCells, inflowU, inflowV);
    } else if (Problem == "FlowOverStep") {
        FlowOverStep(imax, jmax, grid, slipCells, inflowU, inflowV);
    } else if (Problem == "NaturalConvection") {
        NaturalConvection(imax, jmax, grid, T_h, T_c, slipCells);
    } else if (Problem == "FluidTrap") {
        FluidTrap(imax, jmax, grid, T_h, T_c, slipCells);
    } else {
        ERROR("The Problem string doesnt match any of the defined scenarios, please check that its exactly one of the following: \n LidDrivenCavity, PlaneShearFlow, KarmanVortexStreet, FlowOverStep, NaturalConvection, FluidTrap");
    }
}

void SerialExecution() {
    Grid grid = Grid(imax, jmax, 1, PI, UI, VI, TI);
    // apply the geometry data to the grid
    grid.setGeom(geometry);
    //Cells contain: type, values and are linked
    fillList(grid, geometry, slipCells, inFlowCells, outFlowCells, fluidCells, imax, jmax);
    // go through list of boundary cells and check if both west && east or north && south are fluid
    checkGeometry(slipCells);
    // time
    double t = 0;
    double t_file_tracker = 0;
    // iteratioN
    int n = 0;

    //set boundaries initially
    setBoundaries(Problem, imax, jmax, grid, slipCells, inflowU, inflowV, T_h, T_c);

    //print initial state before any computation
    createOutputFile(grid, outFileName, n, geometry, imax, jmax);
    n += 1;


    // Capital F,G for equation 9 10 17
    matrix<double> F;
    matrix<double> G;

    // RHS for Sor iteration
    matrix<double> RS;

    // list for tracking the itmax failures of SOR
    std::list<int> itmaxtracker;
    while (t < t_end) {

        // Select δt according to (13)
        if (dynamicTimestepping) {
            calculate_dt(Re, tau, &dt, dx, dy, PR, grid.getMax(velocity_type::U), grid.getMax(velocity_type::V));
        }
        // Set boundary values for u and v according to (14),(15)

        setBoundaries(Problem, imax, jmax, grid, slipCells, inflowU, inflowV, T_h, T_c);
        std::cout << "Hello2121\n";


        calculate_T(dt, dx, dy, imax, jmax, grid, alpha, PR, Re, slipCells, inFlowCells, outFlowCells);
        std::cout << "Helhhhlo\n";

        // Compute F n and G n according to (9),(10),(17)
        calculate_fg(Re, GX, GY, alpha, dt, dx, dy, imax, jmax, grid, F, G, slipCells, inFlowCells, outFlowCells, beta,
                     1);

        // Compute the right-hand side rhs of the pressure equation (11)
        calculate_rs(dt, dx, dy, imax, jmax, F, G, RS, grid, 1);

        // Set it := 0
        int it = 0;

        // variable for the residual, which is initially larger than epsilon so that we enter the loop
        double res = eps + 1.0;

        //  While it < it max and res > ε
        while (it < itermax && res > eps) {
            // Perform a SOR iteration according to (18) using the provided function and retrieve the residual res
            std::cout << "im alive!!\n";

            sor(omg, dx, dy, imax, jmax, grid, RS, &res, slipCells, inFlowCells, outFlowCells, fluidCells);
            // it := it + 1
            it += 1;
        }


        if (it == itermax) {
            std::cout << "\u001B[31m";
            std::cout << "Warning: SOR did not converge at iteration: " << n << ". The last residual was: "
                      << "\u001B[32m" << res;
            std::cout << "\u001B[0m" << std::endl;
            itmaxtracker.push_back(n);
        }

        // Compute u n+1 and v n+1 according to (7),(8)
        calculate_uv(dt, dx, dy, imax, jmax, grid, F, G, 1);

        // set the boundary again to ensure correct values in the outputfiles (probably only necessary for temperature
        // also, due to changes in calc_uv due to the parallelization WS, this step is now (probably) necessary.
        setBoundaries(Problem, imax, jmax, grid, slipCells, inflowU, inflowV, T_h, T_c);

        // Output of u, v, p values for visualization, if necessary
        if (t_file_tracker > dt_value) {
            createOutputFile(grid, outFileName, n, geometry, imax, jmax);
            // dont completely reset it as this creation of the Outputfile might already be a bit too late and that way we dont accumulate this "little bit too late" error over time.
            // If, for some reason, we get a t_file_tracker more than twice as large as dt_value, we want to reduce it to the modspace dt_value, and not just once.
            while (t_file_tracker > dt_value) {
                t_file_tracker = dt_value - t_file_tracker;
            }

        }

        // increase t
        t += dt;
        t_file_tracker += dt;
        // n := n + 1
        n += 1;

        // output t on console to see simulation progress
        std::cout << "t= " << std::to_string(t) << "\n";
    }
    std::string itmaxMessage = "";
    for (int e: itmaxtracker) {
        itmaxMessage += std::to_string(e) + ", ";
    }
    itmaxMessage = itmaxMessage.substr(0, itmaxMessage.length() - 1);
    std::cout << "Simulation finished, files have been written to " << outFileName << "\n";
    std::cout << "In the following timesteps, the SOR did not terminate within the it_max limit: " << itmaxMessage;

}

void BoundaryLidDrivenCavityParallel(double **U, double **V, int imax, int jmax, int whichboundary, int rank) {
    // 0 = left boundary
    // 1 = right boundary
    // 2 = bottom boundary
    // 3 = top boundary
    assert(0 <= whichboundary && 3 >= whichboundary);
    double Lidspeed = 1;

    switch (whichboundary) {
        case 0:
            //std::cout << "rank " <<rank << " has entered case 0\n";
            V[0][1] = -V[1][1];
            for (int j = 0; j < jmax; j++) {
                U[1][j + 1] = 0;
                V[0][j + 2] = -V[1][j + 2];
            }
            break;
        case 1:
            //std::cout << "rank " <<rank << " has entered case 1\n";
            V[imax + 1][1] = -V[imax][1];
            for (int j = 0; j < jmax; j++) {
                U[imax + 1][j + 1] = 0;
                V[imax + 1][j + 2] = -V[imax][j + 2];
            }
            break;
        case 2:
            //std::cout << "rank " <<rank << " has entered case 2\n";
            U[1][0] = -U[1][1];
            for (int i = 0; i < imax; i++) {
                V[i + 1][1] = 0;
                U[i + 2][0] = -U[i + 2][1];
            }
            break;
        case 3:
            //std::cout << "rank " <<rank << " has entered case 3\n";
            U[1][jmax + 1] = 2 * Lidspeed - U[1][jmax];
            for (int i = 0; i < imax; i++) {
                V[i + 1][jmax + 1] = 0;
                U[i + 2][jmax + 1] = 2 * Lidspeed - U[i + 2][jmax];
            }
            break;

    }

}

double getMaxFromDoublePointer(double **X, int startrow, int endrow, int startcolumn, int endcolumn) {
    double max = X[startrow][startcolumn];
    for (int i = startrow; i <= endrow; i++) {
        for (int j = startcolumn; j <= endcolumn; j++) {
            if (X[i][j] > max) max = X[i][j];
        }
    }
    return max;
}

void exchangeMaxima(double **U, double **V, double &outU, double &outV, int imax, int jmax) {
    double MaxU = getMaxFromDoublePointer(U, 2, imax + 1, 1, jmax);
    double MaxV = getMaxFromDoublePointer(V, 1, imax, 2, jmax + 1);

    double GlobalMaxU;
    double GlobalMaxV;

    MPI_Reduce(&MaxU, &GlobalMaxU, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
    MPI_Reduce(&MaxV, &GlobalMaxV, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

    MPI_Bcast(&GlobalMaxU, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&GlobalMaxV, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    outU = GlobalMaxU;
    outV = GlobalMaxV;
}


void ParallelExecution(int rank) {
    int ir, il, jb, jt, omg_i, omg_j, rank_l, rank_r, rank_b, rank_t, imaxSub, jmaxSub;
    init_parallel(iproc, jproc, imax, jmax, rank, &il, &ir, &jb, &jt, &rank_l, &rank_r, &rank_b, &rank_t, &omg_i,
                  &omg_j, iproc * jproc);

    std::cout << rank << "; " << rank_l << ", " << rank_r << ", " << rank_b << ", " << rank_t << "\n";

    // here imax and jmax mean size, i.e. imaxSub cells in i direction and jmaxSub cells in j direction. Later on we use imax and jmax as indexMarkers
    imaxSub = imax / iproc; //25
    jmaxSub = jmax / jproc; //25

    // Declare all Pointers as per the (*) equation in the worksheet 4
    double **p = initDoublePointerToZero((ir + 1) - (il - 1), (jt + 1) - (jb - 1));//27,27
    double **u = initDoublePointerToZero((ir + 1) - (il - 2), (jt + 1) - (jb - 1));//28,27
    double **v = initDoublePointerToZero((ir + 1) - (il - 1), (jt + 1) - (jb - 2));//27,28
    double **F = initDoublePointerToZero((ir + 1) - (il - 2), (jt + 1) - (jb - 1));//28,27
    double **G = initDoublePointerToZero((ir + 1) - (il - 1), (jt + 1) - (jb - 2));//27,28
    double **RS = initDoublePointerToZero((ir + 0) - (il - 0), (jt + 0) - (jb - 0));//25,25

    for (int i = 1; i < imaxSub ;i++) {
        for (int j = 1; j < jmaxSub ;j++) {
            p[i][j] =PI;
        }
    }

    for (int i = 2; i < imaxSub ;i++) {
        for (int j = 1; j < jmaxSub ;j++) {
            u[i][j] = UI;
        }
    }
    for (int i = 1; i < imaxSub ;i++) {
        for (int j = 2; j < jmaxSub ;j++) {
            u[i][j] = VI;
        }
    }



    // rank by lexicographical ordering
    int lexRank = omg_j * iproc + omg_i;
    assert(lexRank == rank);

    bool leftBoundary = 0;
    bool rightBoundary = 0;
    bool topBoundary = 0;
    bool bottomBoundary = 0;

    leftBoundary = (lexRank % iproc == 0);
    rightBoundary = (lexRank % iproc == iproc - 1);
    topBoundary = (lexRank / iproc == jproc - 1);
    bottomBoundary = (lexRank / iproc == 0);

    double t = 0;
    double t_file_tracker = 0;
    int n = 0;

    // set boundary values for velocity
    if (leftBoundary) {
        BoundaryLidDrivenCavityParallel(u, v, imaxSub, jmaxSub, 0, rank);
    }
    if (rightBoundary) {
        BoundaryLidDrivenCavityParallel(u, v, imaxSub, jmaxSub, 1, rank);
    }
    if (bottomBoundary) {
        BoundaryLidDrivenCavityParallel(u, v, imaxSub, jmaxSub, 2, rank);
    }
    if (topBoundary) {
        BoundaryLidDrivenCavityParallel(u, v, imaxSub, jmaxSub, 3, rank);
    }

    createOutputFileParallel(u, v, p, outFileName, n, geometry, imaxSub, jmaxSub, il, jb);

    n += 1;

    double GlobalMaxU, GlobalMaxV;

    while (t < t_end) {
        exchangeMaxima(u, v, GlobalMaxU, GlobalMaxV, imaxSub, jmaxSub);
        calculate_dt(Re, tau, &dt, dx, dy, PR, GlobalMaxU, GlobalMaxV);

        // set boundary values for velocity
        if (leftBoundary) {
            BoundaryLidDrivenCavityParallel(u, v, imaxSub, jmaxSub, 0, rank);
        }
        if (rightBoundary) {
            BoundaryLidDrivenCavityParallel(u, v, imaxSub, jmaxSub, 1, rank);
        }
        if (bottomBoundary) {
            BoundaryLidDrivenCavityParallel(u, v, imaxSub, jmaxSub, 2, rank);
        }
        if (topBoundary) {
            BoundaryLidDrivenCavityParallel(u, v, imaxSub, jmaxSub, 3, rank);
        }

        ldcp_calculate_fg(Re, GX, GY, alpha, dt, dx, dy, imaxSub, jmaxSub, u, v, F, G, leftBoundary, rightBoundary,
                          topBoundary, bottomBoundary, rank);

        ldcp_calculate_rs(dt, dx, dy, imaxSub, jmaxSub, F, G, RS, rank);

        double Overallres = eps + 1;
        double LocalRes = eps + 1;

        int it = 0;

        while (it < itermax && Overallres > eps) {
            sorParallel(omg, dx, dy, imaxSub, jmaxSub, RS, p, &LocalRes, leftBoundary, rightBoundary, topBoundary,
                        bottomBoundary, il, ir, jb, jt, rank_l, rank_r, rank_b, rank_t, rank);
            exchangeAndCalcRes(rank, LocalRes, Overallres);
            it += 1;

        }

        ldcp_calculate_uv(dt, dx, dy, imaxSub, jmaxSub, F, G, u, v, p);
        uv_comm(u, v, il, ir, jb, jt, rank_l, rank_r, rank_b, rank_t, MPI_STATUS_IGNORE, rank);

        if (t_file_tracker > dt_value) {
            createOutputFileParallel(u, v, p, outFileName, n, geometry, imaxSub, jmaxSub, il, jb);
            // dont completely reset it as this creation of the Outputfile might already be a bit too late and that way we dont accumulate this "little bit too late" error over time.
            // If, for jmaxsome reason, we get a t_file_tracker more than twice as large as dt_value, we want to reduce it to the modspace dt_value, and not just once.
            while (t_file_tracker > dt_value) {
                t_file_tracker = dt_value - t_file_tracker;
            }
        }

        n += 1;
        t_file_tracker += dt;
        t += dt;

        // output t on console to see simulation progress
        if (rank == 0) {
            std::cout << "t= " << std::to_string(t) << "\n";
        }


    }

    /*

    Grid Subdomain = Grid(imaxSub, jmaxSub, 2, PI, UI, VI, TI);

    matrix<int> subgeometry;
    subgeometry.resize(imaxSub + 4, std::vector(jmaxSub + 4, 0));

    // set geometry for the Subdomain, as well as the inner ring of ghost cells to grasp the overall geometry.
    // the outer ring is wall by definition, so it doesnt interfere with anything.
    for (int i = 1; i <= ir - il + 1 + 2; i++) {
        for (int j = 1; j <= jt - jb + 1 + 2; j++) {
            subgeometry[i][j] = geometry[il + i - 2][jb + j - 2];
        }
    }


    int **geoAsPointer = toPointer(subgeometry, imaxSub + 4, jmaxSub + 4);

    Subdomain.setGeom(geoAsPointer);


    fillListSubdomain(Subdomain, geoAsPointer, slipCells, inFlowCells, outFlowCells, fluidCells, imaxSubIndex,
                      jmaxSubIndex);

    TestLists(imaxSubIndex,jmaxSubIndex,Subdomain);

    std::string s;
    for (Cell* c : slipCells) {
        s += std::to_string(c->I);
        s += ", ";
        s+= std::to_string(c->J);
        s +=  " is a boundary cell \n";
    }
    std::cout << rank << ": " << s << "\n";

    checkGeometry(slipCells);

    double t = 0;
    double t_file_tracker = 0;
    int n = 0;

    //boundary conditions
    LidDrivenCavityParallel(rank, iproc, jproc, imaxSubIndex, jmaxSubIndex, Subdomain, inflowU, inflowV);

    createOutputFileParallel(Subdomain, outFileName, n, geometry, imaxSubIndex, jmaxSubIndex, il, jb);

    n += 1;

    // Capital F,G for equation 9 10 17
    matrix<double> F;
    matrix<double> G;

    // RHS for Sor iteration
    matrix<double> RS;

    // list for tracking the itmax failures of SOR
    std::list<int> itmaxtracker;
    while (t < t_end) {

        calculate_dt(Re, tau, &dt, dx, dy, imax, jmax, Subdomain, PR, true);

        //boundary values
        LidDrivenCavityParallel(rank, iproc, jproc, imaxSubIndex, jmaxSubIndex, Subdomain, inflowU, inflowV);

        //calc F and G
        // here imaxSub and jmaxSub are indecies again
        calculate_fg(Re, GX, GY, alpha, dt, dx, dy, imaxSubIndex, jmaxSubIndex, Subdomain, F, G, slipCells, inFlowCells,
                     outFlowCells, beta,
                     rank);

        // calc RHS
        calculate_rs(dt, dx, dy, imaxSubIndex, jmaxSubIndex, F, G, RS, Subdomain,rank);

        double Overallres = eps + 1;
        double LocalRes = eps + 1;

        int it = 0;

        while (it < itermax && Overallres > eps) {
            sorParallel(omg, dx, dy, imaxSubIndex, jmaxSubIndex, Subdomain, RS, &LocalRes, slipCells, inFlowCells,
                        outFlowCells,
                        fluidCells, imaxSub, jmaxSub, il, ir, jb, jt, rank_l, rank_r, rank_b, rank_t, rank);
            exchangeAndCalcRes(rank, LocalRes, Overallres);
            it += 1;
        }


        calculate_uv(dt, dx, dy, imaxSubIndex, jmaxSubIndex, Subdomain, F, G,
                     rank);

        uv_comm_wrapper(Subdomain, imaxSub, jmaxSub, il, ir, jb, jt, rank_l, rank_r, rank_b, rank_t, rank);


        if (t_file_tracker > dt_value) {
            createOutputFileParallel(Subdomain, outFileName, n, geometry, imaxSubIndex, jmaxSubIndex, il, jb);
            // dont completely reset it as this creation of the Outputfile might already be a bit too late and that way we dont accumulate this "little bit too late" error over time.
            // If, for some reason, we get a t_file_tracker more than twice as large as dt_value, we want to reduce it to the modspace dt_value, and not just once.
            while (t_file_tracker > dt_value) {
                t_file_tracker = dt_value - t_file_tracker;
            }
        }
        // increase t
        t += dt;
        t_file_tracker += dt;
        // n := n + 1
        n += 1;

        // output t on console to see simulation progress
        if (rank == 0) {
            std::cout << "t= " << std::to_string(t) << "\n";
        }

    }*/
}


/**
 * The main operation reads the configuration file, initializes the scenario and
 * contains the main loop. So here are the individual steps of the algorithm:
 *
 * - read the program configuration file using read_parameters()
 * - set up the matrices (arrays) needed. Use the predefined matrix<typename> type and give initial values in the constructor.
 * - perform the main loop
 * - at the end: destroy any memory allocated and print some useful statistics
 *
 * The layout of the grid is decribed by the first figure below, the enumeration
 * of the whole grid is given by the second figure. All the unknowns corresond
 * to a two-dimensional degree of freedom layout, so they are not stored in
 * arrays, but in a matrix.

 * @image html grid.jpg
 *
 * @image html whole-grid.jpg
 *
 * Within the main loop, the following steps are required (for some of the 
 * operations, a definition is defined already within uvp.h):
 *
 * - calculate_dt() Determine the maximal time step size.
 * - boundaryvalues() Set the boundary values for the next time step.
 * - calculate_fg() Determine the values of F and G (diffusion and confection).
 *   This is the right hand side of the pressure equation and used later on for
 *   the time step transition.
 * - calculate_rs()
 * - Iterate the pressure poisson equation until the residual becomes smaller
 *   than eps or the maximal number of iterations is performed. Within the
 *   iteration loop the operation sor() is used.
 * - calculate_uv() Calculate the velocity at the next time step.
 */

int main(int argn, char **args) {

    // read inputfile as first argument, cavity100.dat as default
    MPI_Init(&argn, &args);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);


    if (argn > 1) {
        inputFile = args[1];
        if (argn > 2) {
            outFileName = args[2];
            if (argn > 3) {
                dynamicTimestepping = std::stoi(args[3]);
            }
        } else {
            std::cerr << "Please consider providing an output directory!\n";
        }
    } else {
        std::cerr << "Please consider providing an input file!\n";
    }
    outFileName += "/out";

    read_parameters(inputFile, &Re, &UI, &VI, &PI, &GX, &GY, &t_end, &xlength, &ylength, &dt, &dx, &dy, &imax, &jmax,
                    &alpha, &omg, &tau, &itermax, &eps, &dt_value, &Problem, &geomFile, &inflowU, &inflowV, &TI, &PR,
                    &beta, &T_h, &T_c, &iproc, &jproc);
    if (geomFile.empty()) {
        geometry = createBox(imax, jmax);
    } else {
        int geometrySizeX;
        int geometrySizeY;
        // adjust the Path
        geomFile = "../" + geomFile;
        geometry = read_pgm(geomFile.c_str(), &geometrySizeX, &geometrySizeY);
        //check geometry size
        assert(geometrySizeX == imax + 2 && geometrySizeY == jmax + 2);
        //check for geometry thickness

    }
    bool parallelize = 0;
    int MPISize;
    MPI_Comm_size(MPI_COMM_WORLD, &MPISize);
    if (MPISize > 1) {
        parallelize = 1;
        outFileName += "p";
        outFileName += std::to_string(rank);
    }
    assert(iproc * jproc == MPISize);

    if (parallelize && Problem != "LidDrivenCavity") {
        ERROR("you have selected parallelization and a Problem different from LidDrivenCavity. Parallelization is only available for this Problem. Please either choose a different Problem String or set iproc AND jproc to 1");
    }

    if (parallelize) {
        ParallelExecution(rank);
    } else {
        std::cout << "entering Serial\n";
        SerialExecution();
    }
    MPI_Finalize();
    return 0;
}

