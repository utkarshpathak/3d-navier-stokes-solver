#ifndef CFDLAB_CELL_HPP
#define CFDLAB_CELL_HPP

#include <array>
#include "enums.hpp"

class Cell {
public:
    // Constructors
    Cell();

    Cell(double &PI, double &UI, double &VI);

    Cell(double &PI, double &UI, double &VI, double &TI);

    // Get + Set pressure
    double &pressure();

    void set_pressure(double &value);

    double &temperature();

    void set_temperature(double &value);

    // Get + Set velocity
    double &velocity(velocity_type type);

    void set_velocity(double &value, velocity_type type);

    // Get + Set vorder
    bool &border(border_position position);

    void set_border(border_position position);

    int I, J;

private:
    // one pressure value per call
    double _pressure    = 0;
    double _temperature = 0;

    // Fixed size velocity
    std::array<double, 2> _velocity = {0};

    // Fixed number of borCders
    std::array<bool, 4> _border = {
            false}; // This tracks whether a cell is on the domain border, NOT whether a cell is a boundary- cond cell

    cell_type cellType;

    // Pointers to Neighbouring cells. If a cell lies on the border, this points to the "infinity" cell. This is a dummy cell that lets us avoid nullptr checks
    Cell *North;
    Cell *East;
    Cell *South;
    Cell *West;
public:
    cell_type getCellType() const;

    void setCellType(cell_type cellType);

    Cell *getNorth() const;

    void setNorth(Cell *north);

    Cell *getEast() const;

    void setEast(Cell *east);

    Cell *getSouth() const;

    void setSouth(Cell *south);

    Cell *getWest() const;

    void setWest(Cell *west);


#endif //CFDLAB_CELL_HPP
};
