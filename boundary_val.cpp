#include <iostream>
#include "boundary_val.hpp"
#include "datastructures.hpp"
#include "grid.hpp"
#include "list"

//TODO function setVal(cell, veltype, value, whichBoundary) to generalize boundary conditions
void setNoSlipVal(Cell &cell) {
    assert(cell.getCellType() == cell_type::NOSLIP);
    double Zero = 0.0;
    //detect Type of cell
    if (cell.getEast()->getCellType() == cell_type::FLUID) {
        // West is not Fluid
        if (cell.getNorth()->getCellType() == cell_type::FLUID) {
            // Case B_NE
            cell.set_velocity(Zero, velocity_type::U);
            cell.set_velocity(Zero, velocity_type::V);
            double NegSouthEastV = -(cell.getSouth()->getEast()->velocity(velocity_type::V));
            double NegNorthWestU = -(cell.getWest()->getNorth()->velocity(velocity_type::U));
            cell.getSouth()->set_velocity(NegSouthEastV, velocity_type::V);
            cell.getWest()->set_velocity(NegNorthWestU, velocity_type::U);
        } else if (cell.getSouth()->getCellType() == cell_type::FLUID) {
            // Case B_SE
            cell.set_velocity(Zero, velocity_type::U);
            cell.getSouth()->set_velocity(Zero, velocity_type::V);
            double NegSouthWestU = -(cell.getSouth()->getWest()->velocity(velocity_type::U));
            double NegEastV = -(cell.getEast()->velocity(velocity_type::V));
            cell.set_velocity(NegEastV, velocity_type::V);
            cell.getWest()->set_velocity(NegSouthWestU, velocity_type::U);
        } else {
            // Case E
            cell.set_velocity(Zero, velocity_type::U);
            double NegEastV = -cell.getEast()->velocity(velocity_type::V);
            double NegSouthEastV = -cell.getEast()->getSouth()->velocity(velocity_type::V);
            cell.set_velocity(NegEastV, velocity_type::V);
            cell.getSouth()->set_velocity(NegSouthEastV, velocity_type::V);

        }
    } else if (cell.getWest()->getCellType() == cell_type::FLUID) {
        // East is not Fluid
        if (cell.getNorth()->getCellType() == cell_type::FLUID) {
            // Case B_NW
            cell.set_velocity(Zero, velocity_type::V);
            cell.getWest()->set_velocity(Zero, velocity_type::U);
            double NegSouthWestV = -(cell.getWest()->getSouth()->velocity(velocity_type::V));
            double NegNorthU = -(cell.getNorth()->velocity(velocity_type::U));
            cell.set_velocity(NegNorthU, velocity_type::U);
            cell.getSouth()->set_velocity(NegSouthWestV, velocity_type::V);
        } else if (cell.getSouth()->getCellType() == cell_type::FLUID) {
            // Case B_SW
            cell.getWest()->set_velocity(Zero, velocity_type::U);
            cell.getSouth()->set_velocity(Zero, velocity_type::V);
            double NegWestV = -(cell.getWest()->velocity(velocity_type::V));
            double NegSouthU = -(cell.getSouth()->velocity(velocity_type::U));
            cell.set_velocity(NegWestV, velocity_type::V);
            cell.set_velocity(NegSouthU, velocity_type::U);
        } else {
            // Case W
            cell.getWest()->set_velocity(Zero, velocity_type::U);
            double NegWestV = -cell.getWest()->velocity(velocity_type::V);
            double NegSouthWestV = -cell.getWest()->getSouth()->velocity(velocity_type::V);
            cell.set_velocity(NegWestV, velocity_type::V);
            cell.getSouth()->set_velocity(NegSouthWestV, velocity_type::V);
        }
    } else if (cell.getNorth()->getCellType() == cell_type::FLUID) {
        // Case N
        cell.set_velocity(Zero, velocity_type::V);
        double NegNorthU = -cell.getNorth()->velocity(velocity_type::U);
        cell.set_velocity(NegNorthU, velocity_type::U);
        double NegNorthWestU = -cell.getWest()->getNorth()->velocity(velocity_type::U);
        cell.getWest()->set_velocity(NegNorthWestU, velocity_type::U);
    } else if (cell.getSouth()->getCellType() == cell_type::FLUID) {
        // Case S
        cell.getSouth()->set_velocity(Zero, velocity_type::V);
        double NegSouthU = -cell.getSouth()->velocity(velocity_type::U);
        cell.set_velocity(NegSouthU, velocity_type::U);
        double NegSouthWestU = -cell.getWest()->getSouth()->velocity(velocity_type::U);
        cell.getWest()->set_velocity(NegSouthWestU, velocity_type::U);


    } else {
        // No Fluid boundaries
    }
}

void setFreeSlipVal(Cell &cell) {
    assert(cell.getCellType() == cell_type::FREESLIP);
    double Zero = 0.0;
    //detect Type of cell
    if (cell.getEast()->getCellType() == cell_type::FLUID) {
        // West is not Fluid
        if (cell.getNorth()->getCellType() == cell_type::FLUID) {
            // Case B_NE
            cell.set_velocity(Zero, velocity_type::U);
            cell.set_velocity(Zero, velocity_type::V);
        } else if (cell.getSouth()->getCellType() == cell_type::FLUID) {
            // Case B_SE
            cell.set_velocity(Zero, velocity_type::U);
            cell.getSouth()->set_velocity(Zero, velocity_type::V);
        } else {
            // Case E
            cell.set_velocity(Zero, velocity_type::U);
        }
    } else if (cell.getWest()->getCellType() == cell_type::FLUID) {
        // East is not Fluid
        if (cell.getNorth()->getCellType() == cell_type::FLUID) {
            // Case B_NW
            cell.set_velocity(Zero, velocity_type::V);
            cell.getWest()->set_velocity(Zero, velocity_type::U);
        } else if (cell.getSouth()->getCellType() == cell_type::FLUID) {
            // Case B_SW
            cell.getWest()->set_velocity(Zero, velocity_type::U);
            cell.getSouth()->set_velocity(Zero, velocity_type::V);
        } else {
            // Case W
            cell.getWest()->set_velocity(Zero, velocity_type::U);
        }
    } else if (cell.getNorth()->getCellType() == cell_type::FLUID) {
        // Case N
        cell.set_velocity(Zero, velocity_type::V);
    } else if (cell.getSouth()->getCellType() == cell_type::FLUID) {
        // Case S
        cell.getSouth()->set_velocity(Zero, velocity_type::V);
    } else {
        // No Fluid boundaries
    }
}

void PlaneShearFlow(int imax, int jmax, Grid &grid, std::list<Cell *> &cells, double inflowU, double inflowV) {
    for (Cell *c : cells) {
        if (c->getCellType() == cell_type::NOSLIP) {
            setNoSlipVal(*c);
        } else {
            //sanity check
            assert(c->getCellType() == cell_type::FREESLIP);
            setFreeSlipVal(*c);
        }
    }

    // inflow and outflow for plane shear flow
    for (int j = 1; j <= jmax; j++) {
        //sanity check
        assert(grid.cell(0, j).getCellType() == cell_type::INFLOW);
        assert(grid.cell(imax + 1, j).getCellType() == cell_type::OUTFLOW);
        //inflow
        grid.cell(0, j).set_velocity(inflowU, velocity_type::U);
        grid.cell(0, j).set_velocity(inflowV, velocity_type::V);
        //outflow
        double u = grid.cell(imax - 1, j).velocity(velocity_type::U);
        double v = grid.cell(imax, j).velocity(velocity_type::V);

        grid.cell(imax, j).set_velocity(u, velocity_type::U);
        grid.cell(imax + 1, j).set_velocity(v, velocity_type::V);
    }
}

void KarmanVortexStreet(int imax, int jmax, Grid &grid, std::list<Cell *> &cells, double inflowU, double inflowV) {
    for (Cell *c : cells) {
        if (c->getCellType() == cell_type::NOSLIP) {
            setNoSlipVal(*c);
        } else {
            //sanity check
            assert(c->getCellType() == cell_type::FREESLIP);
            setFreeSlipVal(*c);
        }
    }

    // inflow and outflow for plane shear flow
    for (int j = 1; j <= jmax; j++) {
        //sanity check
        assert(grid.cell(0, j).getCellType() == cell_type::INFLOW);
        assert(grid.cell(imax + 1, j).getCellType() == cell_type::OUTFLOW);
        //inflow
        grid.cell(0, j).set_velocity(inflowU, velocity_type::U);
        grid.cell(0, j).set_velocity(inflowV, velocity_type::V);
        //outflow
        double u = grid.cell(imax - 1, j).velocity(velocity_type::U);
        double v = grid.cell(imax, j).velocity(velocity_type::V);

        grid.cell(imax, j).set_velocity(u, velocity_type::U);
        grid.cell(imax + 1, j).set_velocity(v, velocity_type::V);
    }
}

void FlowOverStep(int imax, int jmax, Grid &grid, std::list<Cell *> &cells, double inflowU, double inflowV) {
    for (Cell *c : cells) {
        if (c->getCellType() == cell_type::NOSLIP) {
            setNoSlipVal(*c);
        } else {
            //sanity check
            assert(c->getCellType() == cell_type::FREESLIP);
            setFreeSlipVal(*c);
        }
    }

    // inflow and outflow for plane shear flow
    for (int j = 1; j <= jmax; j++) {
        //sanity check
        if (j > 10) {

            assert(grid.cell(0, j).getCellType() == cell_type::INFLOW);
            double u = inflowU;
            double v = inflowV;
            //inflow
            grid.cell(0, j).set_velocity(u, velocity_type::U);
            grid.cell(0, j).set_velocity(v, velocity_type::V);
        }

        assert(grid.cell(imax + 1, j).getCellType() == cell_type::OUTFLOW);

        //outflow
        double u = grid.cell(imax - 1, j).velocity(velocity_type::U);
        double v = grid.cell(imax, j).velocity(velocity_type::V);

        grid.cell(imax, j).set_velocity(u, velocity_type::U);
        grid.cell(imax + 1, j).set_velocity(v, velocity_type::V);
    }
}

void NaturalConvection(int imax, int jmax, Grid &grid, double T_h, double T_c, std::list<Cell *> slipcells) {
    for (Cell *c : slipcells) {
        if (c->getCellType() == cell_type::NOSLIP) {
            setNoSlipVal(*c);
        } else {
            //sanity check
            assert(c->getCellType() == cell_type::FREESLIP);
            setFreeSlipVal(*c);
        }
    }
    // insulated walls
    for (int i = 1; i <= imax; i++) {
        grid.cell(i, 0).set_temperature(grid.cell(i, 1).temperature());
        grid.cell(i, jmax + 1).set_temperature(grid.cell(i, jmax).temperature());
    }
    // hot and cold wall
    for (int j = 1; j <= jmax; j++) {
        double newTempL = 2 * T_h - grid.cell(1, j).temperature();
        double newTempR = 2 * T_c - grid.cell(imax, j).temperature();
        grid.cell(0, j).set_temperature(newTempL);
        grid.cell(imax + 1, j).set_temperature(newTempR);
    }
}

void FluidTrap(int imax, int jmax, Grid &grid, double T_h, double T_c, std::list<Cell *> slipcells) {
    for (Cell *c : slipcells) {
        if (c->getCellType() == cell_type::NOSLIP) {
            setNoSlipVal(*c);
        } else {
            //sanity check
            assert(c->getCellType() == cell_type::FREESLIP);
            setFreeSlipVal(*c);
        }
    }
    // insulated walls
    for (int i = 1; i <= imax; i++) {
        grid.cell(i, 0).set_temperature(grid.cell(i, 1).temperature());
        grid.cell(i, jmax + 1).set_temperature(grid.cell(i, jmax).temperature());
    }
    // hot and cold wall
    for (int j = 1; j <= jmax; j++) {
        double newTempL = 2 * T_h - grid.cell(1, j).temperature();
        double newTempR = 2 * T_c - grid.cell(imax, j).temperature();
        grid.cell(0, j).set_temperature(newTempL);
        grid.cell(imax + 1, j).set_temperature(newTempR);
    }
}


void boundaryvalues(int imax, int jmax, Grid &grid) {

    std::vector<Cell> Bottom(imax + 2);
    std::vector<Cell> Top(imax + 2);
    std::vector<Cell> ScndTop(imax + 2);
    std::vector<Cell> Left(jmax + 2);
    std::vector<Cell> Right(jmax + 2);
    std::vector<Cell> ScndRight(jmax + 2);

    //TODO maybe only use one vector and use it sequentially

    grid.cells(Bottom, matrix_selection::ROW, 0);
    grid.cells(Top, matrix_selection::ROW, jmax + 1);
    grid.cells(ScndTop, matrix_selection::ROW, jmax);
    grid.cells(Left, matrix_selection::COLUMN, 0);
    grid.cells(Right, matrix_selection::COLUMN, imax + 1);
    grid.cells(ScndRight, matrix_selection::COLUMN, imax);

    double Zero = 0.0;
    for (int i = 0; i < imax + 2; i++) {
        // Bottom boundary conds
        Bottom.at(i).set_velocity(Zero, velocity_type::V);
        double Neg_of_ScndBot_U = -grid.cell(i, 1).velocity(
                velocity_type::U); // as we only read from ScndBot, we access it via grid and dont import the row
        Bottom.at(i).set_velocity(Neg_of_ScndBot_U, velocity_type::U);

        // Top boundary conds
        ScndTop.at(i).set_velocity(Zero, velocity_type::V);
        double Neg_of_ScndTop_U = -ScndTop.at(i).velocity(velocity_type::U);
        Top.at(i).set_velocity(Neg_of_ScndTop_U, velocity_type::U);
    }
    for (int j = 0; j < imax + 2; j++) {
        // left boundary conds
        Left.at(j).set_velocity(Zero, velocity_type::U);
        double Neg_of_ScndLeft_V = -grid.cell(1, j).velocity(
                velocity_type::V); // as we only read from ScndLeft, we access it via grid and dont import the column
        Left.at(j).set_velocity(Neg_of_ScndLeft_V, velocity_type::V);

        // right boundary conds
        ScndRight.at(j).set_velocity(Zero, velocity_type::U);
        double Neg_of_ScndRight_V = -ScndRight.at(j).velocity(velocity_type::V);
        Right.at(j).set_velocity(Neg_of_ScndRight_V, velocity_type::V);
    }

    // Integrate vectors back into the grid
    grid.set_cells(Bottom, matrix_selection::ROW, 0);
    grid.set_cells(Top, matrix_selection::ROW, jmax + 1);
    grid.set_cells(ScndTop, matrix_selection::ROW, jmax);
    grid.set_cells(Left, matrix_selection::COLUMN, 0);
    grid.set_cells(Right, matrix_selection::COLUMN, imax + 1);
    grid.set_cells(ScndRight, matrix_selection::COLUMN, imax);
}

void LidDrivenCavity(int imax, int jmax, Grid &grid, double inflowU, double inflowV) {

    std::vector<Cell> Bottom(imax + 2);
    std::vector<Cell> Top(imax + 2);
    std::vector<Cell> ScndTop(imax + 2);
    std::vector<Cell> Left(jmax + 2);
    std::vector<Cell> Right(jmax + 2);
    std::vector<Cell> ScndRight(jmax + 2);

    grid.cells(Bottom, matrix_selection::ROW, 0);
    grid.cells(Top, matrix_selection::ROW, jmax + 1);
    grid.cells(ScndTop, matrix_selection::ROW, jmax);
    grid.cells(Left, matrix_selection::COLUMN, 0);
    grid.cells(Right, matrix_selection::COLUMN, imax + 1);
    grid.cells(ScndRight, matrix_selection::COLUMN, imax);

    double Zero = 0.0;
    for (int i = 0; i < imax + 2; i++) {
        // Bottom boundary conds
        Bottom.at(i).set_velocity(Zero, velocity_type::V);
        double Neg_of_ScndBot_U = -grid.cell(i, 1).velocity(
                velocity_type::U); // as we only read from ScndBot, we access it via grid and dont import the row
        Bottom.at(i).set_velocity(Neg_of_ScndBot_U, velocity_type::U);

        // Top boundary conds
        ScndTop.at(i).set_velocity(inflowV, velocity_type::V);
        double Neg_of_ScndTop_U_moving = 2 * inflowU - ScndTop.at(i).velocity(velocity_type::U);
        Top.at(i).set_velocity(Neg_of_ScndTop_U_moving, velocity_type::U);
    }
    for (int j = 1; j < imax + 1; j++) {
        // left boundary conds
        Left.at(j).set_velocity(Zero, velocity_type::U);
        double Neg_of_ScndLeft_V = -grid.cell(1, j).velocity(
                velocity_type::V); // as we only read from ScndLeft, we access it via grid and dont import the column
        Left.at(j).set_velocity(Neg_of_ScndLeft_V, velocity_type::V);

        // right boundary conds
        ScndRight.at(j).set_velocity(Zero, velocity_type::U);
        double Neg_of_ScndRight_V = -ScndRight.at(j).velocity(velocity_type::V);
        Right.at(j).set_velocity(Neg_of_ScndRight_V, velocity_type::V);
    }

    // Integrate vectors back into the grid
    grid.set_cells(Bottom, matrix_selection::ROW, 0);
    grid.set_cells(Top, matrix_selection::ROW, jmax + 1);
    grid.set_cells(ScndTop, matrix_selection::ROW, jmax);
    grid.set_cells(Left, matrix_selection::COLUMN, 0);
    grid.set_cells(Right, matrix_selection::COLUMN, imax + 1);
    grid.set_cells(ScndRight, matrix_selection::COLUMN, imax);
}


void LidDrivenCavityParallel(int rank, int iproc, int jproc, int imax, int jmax, Grid &subGrid, double inflowU,
                             double inflowV) {

    // Note: imax and jmax are not the number of cells in the respective direction but instead they give the index at which the last cell
    // of the actual fluid domain in each direction is located.
    // e.g. if you have 25 cells per subdomain imax will contain 26 as the columns with index 1 and 0 are both ghost cells

    double zero = 0.0;
    if (rank % iproc == 0) {
        // left wall
        for (int j = 2; j <= jmax; j++) {
            subGrid.cell(1, j).set_velocity(zero, velocity_type::U);
            double negV = -subGrid.cell(2, j).velocity(velocity_type::V);
            subGrid.cell(1, j).set_velocity(negV, velocity_type::V);
        }
    }
    if (rank % iproc == iproc - 1) {
        // right wall
        for (int j = 2; j <= jmax; j++) {
            subGrid.cell(imax, j).set_velocity(zero, velocity_type::U);
            double negV = -subGrid.cell(imax, j).velocity(velocity_type::V);
            subGrid.cell(imax + 1, j).set_velocity(negV, velocity_type::V);
        }
    }
    if (rank / iproc == 0) {
        // bottom wall
        for (int i = 2; i <= imax; i++) {
            subGrid.cell(i, 1).set_velocity(zero, velocity_type::V);
            double negU = -subGrid.cell(i, 2).velocity(velocity_type::U);
            subGrid.cell(i, 1).set_velocity(negU, velocity_type::U);
        }
    }
    if (rank / iproc == jproc - 1) {
        //top wall
        for (int i = 0; i <= imax + 2; i++) {
            subGrid.cell(i, jmax).set_velocity(zero, velocity_type::V);
            double LidU = 2 * inflowU - subGrid.cell(i, jmax).velocity(velocity_type::U);
            subGrid.cell(i, jmax + 1).set_velocity(LidU, velocity_type::U);
        }

    }


}

/*
 * if (cell.getEast()->getCellType() == cell_type::FLUID) {
        // West is not Fluid
        if (cell.getNorth()->getCellType() == cell_type::FLUID) {
            // Case B_NE
        }
        else if (cell.getSouth()->getCellType() == cell_type::FLUID) {
            // Case B_SE
        }
        else {
            // Case E
        }
    } else if (cell.getWest()->getCellType() == cell_type::FLUID) {
        // East is not Fluid
        if (cell.getNorth()->getCellType() == cell_type::FLUID) {
            // Case B_NW
        }
        else if (cell.getSouth()->getCellType() == cell_type::FLUID) {
            // Case B_SW
        } else {
            // Case W
        }
    } else if (cell.getNorth()->getCellType() == cell_type::FLUID) {
        // Case N
    } else if (cell.getSouth()->getCellType() == cell_type::FLUID) {
        // Case S

    } else {
        // No Fluid boundaries
    }
 */
