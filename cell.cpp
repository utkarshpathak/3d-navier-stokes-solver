#include "cell.hpp"

Cell::Cell() {};

Cell::Cell(double &PI, double &UI, double &VI) :
        _pressure(PI) {
    set_velocity(UI, velocity_type::U);
    set_velocity(VI, velocity_type::V);
};

Cell::Cell(double &PI, double &UI, double &VI,double &TI) :
        _pressure(PI){
    set_velocity(UI, velocity_type::U);
    set_velocity(VI, velocity_type::V);
    set_temperature(TI);
};

// Pressure Get and Set
double &Cell::pressure() {
    return _pressure;
}

void Cell::set_pressure(double &value) {
    _pressure = value;
}

double &Cell::temperature() {
    return _temperature;
}

void Cell::set_temperature(double &value) {
    _temperature = value;
}

// Velocity Get and Set
double &Cell::velocity(velocity_type type) {
    return _velocity[static_cast<int>(type)];
};

void Cell::set_velocity(double &value, velocity_type type) {
    _velocity[static_cast<int>(type)] = value;
};

// borders Get and Set
bool &Cell::border(border_position position) {
    return _border[static_cast<int>(position)];
};

// Set border
void Cell::set_border(border_position position) {
    _border[static_cast<int>(position)] = true;
}

cell_type Cell::getCellType() const {
    return cellType;
}

void Cell::setCellType(cell_type cellType) {
    Cell::cellType = cellType;
}

Cell *Cell::getNorth() const {
    return North;
}

void Cell::setNorth(Cell *north) {
    North = north;
}

Cell *Cell::getEast() const {
    return East;
}

void Cell::setEast(Cell *east) {
    East = east;
}

Cell *Cell::getSouth() const {
    return South;
}

void Cell::setSouth(Cell *south) {
    South = south;
}

Cell *Cell::getWest() const {
    return West;
}

void Cell::setWest(Cell *west) {
    West = west;
};
