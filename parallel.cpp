//
// Created by max on 02.06.20.
//

#include "parallel.h"
#include "helper.hpp"
#include "mpi.h"
#include "datastructures.hpp"

void init_parallel(int iproc, int jproc, int imax, int jmax,
                   int myrank, int *il, int *ir, int *jb, int *jt,
                   int *rank_l, int *rank_r, int *rank_b, int *rank_t, int *omg_i, int *omg_j, int num_proc) {

    if (myrank == 0) {
        if (imax % iproc != 0) {
            ERROR("The selected grid size isn't divisible by the desired number of processes in x-direction. Please adjust imax in the Inputfile s.t. it's a multiple of iproc");
        }
        if (jmax % jproc != 0) {
            ERROR("The selected grid size isn't divisible by the desired number of processes in y-direction. Please adjust jmax in the Inputfile s.t. it's a multiple of jproc");
        }
        assert(imax % iproc == 0 && jmax % jproc == 0);
        int subdomainsizeI = imax / iproc;
        int subdomainsizeJ = jmax / jproc;
        for (int j = 0; j < jproc; j++) {
            for (int i = 0; i < iproc; i++) {
                if (i + j == 0) {
                    // values for the master process
                    *omg_i = 0;
                    *omg_j = 0;
                    *il = 0;
                    *ir = subdomainsizeI;
                    *jb = 0;
                    *jt = subdomainsizeJ;
                    *rank_l = MPI_PROC_NULL;
                    if (iproc != 1) {
                        *rank_r = 1;
                    } else {
                        *rank_r = MPI_PROC_NULL;
                    }
                    *rank_b = MPI_PROC_NULL;
                    if (jproc != 1) {
                        *rank_t = iproc;
                    } else {
                        *rank_t = MPI_PROC_NULL;
                    }


                    continue;
                }

                int data[10];

                int new_omg_i = i;
                int new_omg_j = j;
                int new_il = new_omg_i * subdomainsizeI;
                int new_ir = (new_omg_i + 1) * subdomainsizeI;
                int new_jb = new_omg_j * subdomainsizeJ;
                int new_jt = (new_omg_j + 1) * subdomainsizeJ;
                int new_rank_l, new_rank_r, new_rank_b, new_rank_t;
                // rank by lexicographical ordering
                int new_proc_rank = j * iproc + i;
                if (new_proc_rank % iproc == 0) {
                    // left OverallDomain border
                    new_rank_l = MPI_PROC_NULL;
                } else {
                    new_rank_l = new_proc_rank - 1;
                }
                if (new_proc_rank % iproc == iproc - 1) {
                    // right OverallDomain border
                    new_rank_r = MPI_PROC_NULL;
                }
                else{
                    new_rank_r = new_proc_rank + 1;
                }
                //neither of the above
                if (new_proc_rank / iproc == 0) {
                    // bottom OverallDomain border
                    new_rank_b = MPI_PROC_NULL;}
                else {
                    new_rank_b = new_proc_rank - iproc;
                }
                if (new_proc_rank / iproc == jproc - 1) {
                    // Top OverallDomain border
                    new_rank_t = MPI_PROC_NULL;
                } else {
                    new_rank_t = new_proc_rank + iproc;
                }

                // fill data
                data[0] = new_omg_i;
                data[1] = new_omg_j;
                data[2] = new_il;
                data[3] = new_ir;
                data[4] = new_jb;
                data[5] = new_jt;
                data[6] = new_rank_l;
                data[7] = new_rank_r;
                data[8] = new_rank_b;
                data[9] = new_rank_t;

                MPI_Send(data, 10, MPI_INT, new_proc_rank, 1, MPI_COMM_WORLD);

            }
        }

    } else {
        int data[10];

        MPI_Recv(data, 10, MPI_INT, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        *omg_i = data[0];
        *omg_j = data[1];
        *il = data[2];
        *ir = data[3];
        *jb = data[4];
        *jt = data[5];
        *rank_l = data[6];
        *rank_r = data[7];
        *rank_b = data[8];
        *rank_t = data[9];
    }

}

void pressure_comm(double **P, int il, int ir, int jb, int jt,
                   int rank_l, int rank_r, int rank_b, int rank_t, MPI_Status *status, int rank) {
    int imax = ir - il;
    int jmax = jt - jb;


    //data, which we send to neighbouring processes
    double LeftRowP[jmax];
    double RightRowP[jmax];
    double TopRowP[imax];
    double BottomRowP[imax];

    //buffers for the data, which we recieve from neighbouring processes
    double new_LeftRowP[jmax];
    double new_RightRowP[jmax];
    double new_TopRowP[imax];
    double new_BottomRowP[imax];

    //fill up the to-send data:
    for (int i = 1; i <= imax; i++) {
        BottomRowP[i - 1] = P[i][1];
        TopRowP[i - 1] = P[i][jmax];

        //safty fillup of rec buffers
        new_BottomRowP[i - 1] = P[i][0];
        new_TopRowP[i - 1] = P[i][jmax + 1];
    }

    for (int j = 1; j <= jmax; j++) {
        LeftRowP[j - 1] = P[1][j];
        RightRowP[j - 1] = P[imax][j];

        //safty fillup of rec buffers
        new_LeftRowP[j - 1] = P[0][j];
        new_RightRowP[j - 1] = P[imax + 1][j];
    }
    std::string s;

    for (int j = 0; j <= jmax - 1; j++) {
        s += std::to_string(RightRowP[j]);
        s += "\n";
    }
    /*if(rank ==2) {
        std::cout << rank << ", sending to " << rank_r << "\n" << s;
    }*/

    MPI_Sendrecv(LeftRowP, jmax, MPI_DOUBLE, rank_l, 1, new_RightRowP, jmax, MPI_DOUBLE, rank_r, 1,
                 MPI_COMM_WORLD, status);

    MPI_Sendrecv(RightRowP, jmax, MPI_DOUBLE, rank_r, 2, new_LeftRowP, jmax, MPI_DOUBLE, rank_l, 2,
                 MPI_COMM_WORLD, status);

    MPI_Sendrecv(TopRowP, imax, MPI_DOUBLE, rank_t, 3, new_BottomRowP, imax, MPI_DOUBLE, rank_b, 3,
                 MPI_COMM_WORLD, status);

    MPI_Sendrecv(BottomRowP, imax, MPI_DOUBLE, rank_b, 4, new_TopRowP, imax, MPI_DOUBLE, rank_t, 4,
                 MPI_COMM_WORLD, status);


    s = "";
    for (int j = 0; j <= jmax - 1; j++) {
        s += std::to_string(new_LeftRowP[j]);
        s += "\n";
    }
/*
    if(rank == 3) {
        std::cout << rank << ": recieving from "<< rank_l << "\n" << s;
    }*/



    // fill recieved data back into thParallelExecution(rank);e matrix

    for (int i = 1; i <= imax; i++) {
        //bottom boundary row
        P[i][0] = new_BottomRowP[i - 1];
        //top boundary row
        P[i][jmax + 1] = new_TopRowP[i - 1];
    }

    for (int j = 1; j <= jmax; j++) {
        //left boundary column
        P[0][j] = new_LeftRowP[j - 1];
        //right boundary column
        P[imax + 1][j] = new_RightRowP[j - 1];
    }


}

void uv_comm(double **U, double **V, int il, int ir, int jb, int jt, int rank_l, int rank_r, int rank_b,
             int rank_t,
             MPI_Status *status, int rank) {

    int imax = ir - il;
    int jmax = jt - jb;

    //send buffers
    double RightInnerRowU[jmax];
    double TopRowU[imax + 2];
    double BottomRowU[imax + 2];
    double LeftRowU[jmax];
    double RightOuterRowU[jmax];

    double TopInnerRowV[imax];
    double BottomRowV[imax];
    double LeftRowV[jmax + 2];
    double RightRowV[jmax + 2];
    double TopOuterRowV[imax];


    //recieve buffers
    double new_LeftInnerRowU[jmax];
    double new_TopRowU[imax + 2];
    double new_BottomRowU[imax + 2];
    double new_LeftOuterRowU[jmax];
    double new_RightRowU[jmax];

    double new_LeftRowV[jmax + 2];
    double new_RightRowV[jmax + 2];
    double new_TopRowV[imax];
    double new_BottomInnerRowV[imax];
    double new_BottomOuterRowV[imax];

    // fill up send buffers

    //Horizontal U
    for (int i = 0; i <= imax+1; i++) {
        BottomRowU[i] = U[i][1];
        TopRowU[i] = U[i][jmax];

        new_TopRowU[i] = U[i][jmax + 1];
        new_BottomRowU[i] = U[i][0];
    }
    //Horizontal V
    for (int i = 1; i <= imax; i++) {
        BottomRowV[i - 1] = V[i][2];
        TopOuterRowV[i - 1] = V[i][jmax + 1];
        TopInnerRowV[i - 1] = V[i][jmax];

        new_TopRowV[i - 1] = V[i][jmax + 2];
        new_BottomInnerRowV[i - 1] = V[i][1];
        new_BottomOuterRowV[i - 1] = V[i][0];
    }

    //Vertical U
    for (int j = 1; j <= jmax; j++) {
        LeftRowU[j - 1] = U[2][j];
        RightOuterRowU[j - 1] = U[imax + 1][j];
        RightInnerRowU[j - 1] = U[imax][j];

        new_RightRowU[j - 1] = U[imax + 2][j];
        new_LeftInnerRowU[j - 1] = U[1][j];
        new_LeftOuterRowU[j - 1] = U[0][j];
    }

    //Vertical V
    for (int j = 0; j <= jmax+1; j++) {
        LeftRowV[j] = V[1][j];
        RightRowV[j] = V[imax][j];

        new_LeftRowV[j] = V[0][j];
        new_RightRowV[j] = V[imax + 1][j];
    }


    // send order: similar to pressure but first U  inner to outer, then V inner to outer
    /*
    std::string s;
    std::string t;


    for (int j=0; j <= jmax+2;j++) {
        s += std::to_string(RightOuterRowU[j]);
        s += "\n";
        t += std::to_string(LeftOuterRowU[j]);
        t += "\n";
    }
    if(rank ==2) {
        std::cout << rank << ":\n" << s;
    }*/



    //send to the left recieve from the right
    MPI_Sendrecv(LeftRowU, jmax, MPI_DOUBLE, rank_l, 1, new_RightRowU, jmax, MPI_DOUBLE, rank_r, 1,
                 MPI_COMM_WORLD, status);
    MPI_Sendrecv(LeftRowV, jmax + 2, MPI_DOUBLE, rank_l, 2, new_RightRowV, jmax + 2, MPI_DOUBLE, rank_r, 2,
                 MPI_COMM_WORLD, status);

    //send to the right recieve from the left
    MPI_Sendrecv(RightInnerRowU, jmax, MPI_DOUBLE, rank_r, 3, new_LeftOuterRowU, jmax, MPI_DOUBLE, rank_l, 3,
                 MPI_COMM_WORLD, status);
    MPI_Sendrecv(RightOuterRowU, jmax, MPI_DOUBLE, rank_r, 4, new_LeftInnerRowU, jmax, MPI_DOUBLE, rank_l, 4,
                 MPI_COMM_WORLD, status);
    MPI_Sendrecv(RightRowV, jmax + 2, MPI_DOUBLE, rank_r, 5, new_LeftRowV, jmax + 2, MPI_DOUBLE, rank_l, 5,
                 MPI_COMM_WORLD, status);

    //send to the Top, recieve from the bottom
    MPI_Sendrecv(TopInnerRowV, imax, MPI_DOUBLE, rank_t, 6, new_BottomOuterRowV, imax, MPI_DOUBLE, rank_b, 6,
                 MPI_COMM_WORLD, status);
    MPI_Sendrecv(TopOuterRowV, imax, MPI_DOUBLE, rank_t, 7, new_BottomInnerRowV, imax, MPI_DOUBLE, rank_b, 7,
                 MPI_COMM_WORLD, status);
    MPI_Sendrecv(TopRowU, imax + 2, MPI_DOUBLE, rank_t, 8, new_BottomRowU, imax + 2, MPI_DOUBLE, rank_b, 8,
                 MPI_COMM_WORLD, status);

    //send to the bottom, recieve from the Top
    MPI_Sendrecv(BottomRowU, imax + 2, MPI_DOUBLE, rank_b, 9, new_TopRowU, imax + 2, MPI_DOUBLE, rank_t, 9,
                 MPI_COMM_WORLD, status);
    MPI_Sendrecv(BottomRowV, imax, MPI_DOUBLE, rank_b, 10, new_TopRowV, imax, MPI_DOUBLE, rank_t, 10,
                 MPI_COMM_WORLD, status);

    /*
    s = "LeftInner\n";
    t= "RightInner\n";
    for (int j=0; j <= jmax+2;j++) {
        s += std::to_string(new_LeftInnerRowU[j]);
        s += "\n";
        t += std::to_string(new_RightInnerRowU[j]);
        t += "\n";
    }

    if(rank == 3) {
        std::cout << rank << ":\n" << s;
    }

     */

    //fill in recieved data
    //Horizontal U
    for (int i = 0; i <= imax+1; i++) {
        U[i][0] = new_BottomRowU[i];
        U[i][jmax + 1] = new_TopRowU[i];
    }
    //Horizontal V
    for (int i = 1; i <= imax; i++) {
        V[i][0] = new_BottomOuterRowV[i - 1];
        V[i][1] = new_BottomInnerRowV[i - 1];
        V[i][jmax + 2] = new_TopRowV[i - 1];
    }

    //vertical U
    for (int j = 1; j <= jmax; j++) {
        U[0][j] = new_LeftOuterRowU[j - 1];
        U[1][j] = new_LeftInnerRowU[j - 1];
        U[imax + 2][j] = new_RightRowU[j - 1];
    }

    //vertical V
    for (int j = 0; j <= jmax+1; j++) {
        V[0][j] = new_LeftRowV[j];
        V[imax + 1][j] = new_RightRowV[j];
    }



}

