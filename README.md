# 3D Navier Stokes solver: 

Code for the CFD Lab taught at TUM Informatics

This repository contains:

* an example input file (`cavity100.dat`), with `100` stemming from the `itermax`
* the headers
* the files with the respective method stubs

Please [fork this repository and add us as collaborators](https://gitlab.lrz.de/tum-i05/public/cfdlabcodeskeleton/-/wikis/home).

Find more information in the [documentation](https://tum-i05.pages.gitlab.lrz.de/public/cfdlabcodeskeleton/).

## Software Requirements

* VTK 7 or higher
* GCC 9 (optional) 

### GCC version

You can get you current version of GCC by running:

```shell
g++ -v
```

### Defining your GCC version

If you have GCC 9 or newer, you can set in the `CMakeLists.txt` file:

```cmake
set(gpp9 True)
```

If you have a version lower than 9, then you don't have to modify the `CMakeLists.txt` file.

This will affect how we are using the C++ filesystem library, which is available already in GCC 7 as an experimental feature.

### Setup of VTK and GCC 9 (Ubuntu **20.04**)

```
apt-get update &&
apt-get upgrade -y &&
apt-get install -y build-essential cmake libvtk7-dev libfmt-dev
```

### Setup of VTK and GCC 9 (Ubuntu **18.04**)

If you want, you can upgrade your compiler version to have access to more recent C++ features.
This is, however, optional.

```
apt-get update &&
apt-get install -y software-properties-common &&
add-apt-repository -y ppa:ubuntu-toolchain-r/test &&
apt-get upgrade -y &&
apt-get install -y build-essential cmake libvtk7-dev libfmt-dev gcc-9 g++-9
apt-get install -y gcc-9 g++-9
```

## Using CMake

CMake is a C++ build system generator, which simplifies the building process compared e.g. to a system-specific Makefile. The CMake configuration is defined in the `CMakeList.txt` file.

In order to build your code with CMake, you can follow this (quite common) procedure:

1. Create a build directory: `mkdir build`
2. Get inside it: `cd build`
3. Configure and generate the build system: `cmake ..` (Note the two dots, this means that the `CmakeLists.txt` File is in the folder above)
4. Build your code: `make` (build the executable)

### Troubleshooting: VTK not found

You might run into a problem where the VTK library is not found. To fix this, you can try the following steps:

1. Find the installation path of your VTK library 
2. Define this path as an environment variable, as e.g. `export VTK_DIR=".../lib/cmake/vtk-8.2"`
3. Start in a clean build folder
4. Run `cmake ..` again

### Set a different GCC version

If you have multiple compiler versions installed you can set the GCC version which should be used by `cmake` like this:

```shell
export CXX=`which g++-7`
```

Make sure to use a backtick (\`) to get the `which` command executed. Afterwards, you can run `cmake ..`.

## Testing 

The folder `tests` contains example files for simple unit testing with the [Catch2](https://github.com/catchorg/Catch2) unit testing framework. The tests can be compiled using `cmake` as well.

After building, run:

```
ctest --verbose
```

With the `verbose` option you can get more details for failing tests.

## Documentation 

The generate the documentation, run:

```
pip3 install --user mkdocs mkdocs-material
```

and then serve it to be able to access it through your browser:

```
mkdocs serve
```

## Usage
The main executable `sim` takes up to 3 arguments. The first one is the input file, the second one the output directory (without the last slash!) and the third one is a boolean flag whether dynamic timestepping should be enabled.
The input file should contain a Path to a geometry File **relative to the main directory**(described by a line "geomFile abc.pmg\n"). 
If this path is not provided we create an empty box. 
In any case there should be a line "geomFile [A-z]*" in the input file 
(followed by nothing if you intend to not provide a geometry file).

For now, we expect a 1 to 1 match of the geometry file(which includes the ghost cells) to the grid. We will throw an error otherwise.
This not only allows for a simpler implementation, but also gives confidence for the User on how the grid **exactly** looks.

Further, adding Temperature now requires the old input files to have Lines for the new Parameters aswell, so files like KarmanVortexStreet500.dat have to be extended by the quantities T_h, T_c, TI, PR and beta. similarly the temperature Scenarios need lines for inflowU and inflowV.
In case you get a SegFault its likely that either the name of the input file is wrong or you are missing some of the quanteties in the input file. In any case you can use our InputFiles to test the execution.

One other thing to look out for is the Problem String in the input file. This one is used to select the correct boundary conditions by string comparison, hence its very sensitive to errors. We throw an error if the given String is not one of the possibilities.
Select one of the following:
* LidDrivenCavity
* PlaneShearFlow 
* KarmanVortexStreet
* FlowOverStep
* NaturalConvection
* FluidTrap


-----ReadMeUpdate WS3-----
First of all Input files are now required to provide values for iproc and jproc, i.e. the number of desired processes in the respective direction.

Further, due to some interesting events involving an index Bug, we currently have 2 working versions of our code.
The one you are looking right now is a more C styled approach, which can be noticed by the significant runtime reduction.
This approach does work for the Lid Driven Cavity but for the Lid Driven cavity only.

The other one, which is available on the branch "WorkingCplusPlusApproach" is using all the structures and functions that were available in WS1 and WS2.
This comes at the cost of a much slower runtime, but also allows us to include geometries. We tried this for the Karman Vortex street and it seemed to work.
No guarantees there, however. :)


Usage Example:
```
./sim ../InputFiles/NaturalConvection.dat ../output
```

Usage Example for Parallel execution:
```
mpirun -np 4 ./sim ../InputFiles/LidDrivenCavity.dat ../output 
```
Make sure that the number of provided processes (-np) matches up with the iproc and jproc of the input File.

