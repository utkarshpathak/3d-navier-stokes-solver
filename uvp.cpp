#include "uvp.hpp"
#include "helper.hpp"
#include "datastructures.hpp"
#include <cmath>
#include <cstdlib>
#include "list"
#include "mpi.h"
#include "parallel.h"



//-----------------------------------------------------------------------------
// Added functions
//-----------------------------------------------------------------------------

//loads and loads of helper functions for finite differences operators

//gets u_i_j from grid
double u(int i, int j, Grid &grid) {
    return grid.cell(i, j).velocity(velocity_type::U);
}

//gets v_i_j from grid
double v(int i, int j, Grid &grid) {
    return grid.cell(i, j).velocity(velocity_type::V);
}

//gets p_i_j from grid
double p(int i, int j, Grid &grid) {
    return grid.cell(i, j).pressure();
}

double T(int i, int j, matrix<double> Told) {
    return Told.at(i).at(j);
}

// all operators from Eq (4)
double u2byx(int i, int j, Grid &grid, double dx, double gammaFactor) {
    double uij = u(i, j, grid);
    double uiplus1j = u(i + 1, j, grid);
    double uiminus1j = u(i - 1, j, grid);
    double firstSummand = (1 / dx) * (pow((uij + uiplus1j) / 2, 2) -
                                      pow((uiminus1j + uij) / 2, 2));
    double scndSummand = gammaFactor * (1 / dx) *
                         ((abs(uij + uiplus1j) / 2) * ((uij - uiplus1j) / 2)
                          - (abs(uiminus1j + uij) / 2) * ((uiminus1j - uij) / 2)); // may be fabs() instead of abs()?
    return firstSummand + scndSummand;
}

double uvbyy(int i, int j, Grid &grid, double dy, double gammaFactor) {
    double uij = u(i, j, grid);
    double uijplus1 = u(i, j + 1, grid);
    double uijminus1 = u(i, j - 1, grid);
    double vij = v(i, j, grid);
    double viplus1j = v(i + 1, j, grid);
    double viplus1jminus1 = v(i + 1, j - 1, grid);
    double vijminus1 = v(i, j - 1, grid);

    double firstSummandPreMinus = ((vij + viplus1j) / 2) * ((uij + uijplus1) / 2);
    double firstSummandPostMinus =
            ((vijminus1 + viplus1jminus1) / 2) * ((uijminus1 + uij) / 2);
    double firstSummand = (1 / dy) * (firstSummandPreMinus - firstSummandPostMinus);
    double scndSummandPreMinus =
            (abs(vij + viplus1j) / 2) * ((uij - uijplus1) / 2);
    double scndSummandPostMinus =
            (abs(vijminus1 + viplus1jminus1) / 2) * ((uijminus1 - uij) / 2);
    double scndSummand = gammaFactor * (1 / dy) * (scndSummandPreMinus + scndSummandPostMinus);
    return firstSummand + scndSummand;
}

double ubyx2(int i, int j, Grid &grid, double dx) {
    return (u(i + 1, j, grid) - 2 * u(i, j, grid) + u(i - 1, j, grid)) / (dx * dx);
}

double ubyy2(int i, int j, Grid &grid, double dy) {
    return (u(i, j + 1, grid) - 2 * u(i, j, grid) + u(i, j - 1, grid)) / (dy * dy);
}

double pbyx(int i, int j, Grid &grid) {
    return (p(i + 1, j, grid) - p(i, j, grid)) / 2;
}

// all operators from Eq (5)
double v2byy(int i, int j, Grid &grid, double dy, double gammaFactor) {
    double vij = v(i, j, grid);
    double vijplus1 = v(i, j + 1, grid);
    double vijminus1 = v(i, j - 1, grid);

    double firstSummand = (1 / dy) * (pow((vij + vijplus1) / 2, 2) -
                                      pow((vijminus1 + vij) / 2, 2));
    double scndSummand = gammaFactor * (1 / dy) *
                         ((abs(vij + vijplus1) / 2) * ((vij - vijplus1) / 2)
                          - (abs(vijminus1 + vij) / 2) * ((vijminus1 - vij) / 2));
    return firstSummand + scndSummand;
}

double uvbyx(int i, int j, Grid &grid, double dx, double gammaFactor) {
    double uij = u(i, j, grid);
    double uiminus1jplus1 = u(i - 1, j + 1, grid);
    double uiminus1j = u(i - 1, j, grid);
    double uijplus1 = u(i, j + 1, grid);
    double vij = v(i, j, grid);
    double viplus1j = v(i + 1, j, grid);
    double viminus1j = v(i - 1, j, grid);

    double firstSummandPreMinus = ((uij + uijplus1) / 2) * ((vij + viplus1j) / 2);
    double firstSummandPostMinus =
            ((uiminus1j + uiminus1jplus1) / 2) * ((viminus1j + vij) / 2);
    double firstSummand = (1 / dx) * (firstSummandPreMinus - firstSummandPostMinus);
    double scndSummandPreMinus =
            (abs(uij + uijplus1) / 2) * ((vij - viplus1j) / 2);
    double scndSummandPostMinus =
            (abs(uiminus1j + uiminus1jplus1) / 2) * ((viminus1j - vij) / 2);
    double scndSummand = gammaFactor * (1 / dx) * (scndSummandPreMinus + scndSummandPostMinus);
    return firstSummand + scndSummand;
}

double vbyx2(int i, int j, Grid &grid, double dx) {
    return (v(i + 1, j, grid) - 2 * v(i, j, grid) + v(i - 1, j, grid)) / (dx * dx);
}

double vbyy2(int i, int j, Grid &grid, double dy) {
    return (v(i, j + 1, grid) - 2 * v(i, j, grid) + v(i, j - 1, grid)) / (dy * dy);
}

double pbyy(int i, int j, Grid &grid) {
    return (p(i, j + 1, grid) - p(i, j, grid)) / 2;
}


// all operators from Eq (6)
double ubyx(int i, int j, Grid &grid) {
    return (u(i, j, grid) - u(i - 1, j, grid)) / 2;
}

double vbyy(int i, int j, Grid &grid) {
    return (v(i, j, grid) - v(i, j - 1, grid)) / 2;
}

// Operators for temperature
double uTbyx(int i, int j, Grid &grid, double dx, double gammafactor, matrix<double> Told) {
    double Firstsummand = (1 / dx) * (u(i, j, grid) * ((T(i, j, Told) + T(i + 1, j, Told)) / 2) -
                                      u(i - 1, j, grid) * ((T(i - 1, j, Told) + T(i, j, Told)) / 2));
    double SecondSummand = (gammafactor / dx) * (abs(u(i, j, grid)) * ((T(i, j, Told) - T(i + 1, j, Told)) / 2) -
                                                 (abs(u(i - 1, j, grid)) * (T(i - 1, j, Told) - T(i, j, Told)) / 2));
    return Firstsummand + SecondSummand;

}

double vTbyy(int i, int j, Grid &grid, double dy, double gammafactor, matrix<double> Told) {
    double Firstsummand = (1 / dy) * (v(i, j, grid) * ((T(i, j, Told) + T(i, j + 1, Told)) / 2) -
                                      v(i, j - 1, grid) * ((T(i, j - 1, Told) + T(i, j, Told)) / 2));
    double SecondSummand = (gammafactor / dy) * (abs(v(i, j, grid)) * ((T(i, j, Told) - T(i, j + 1, Told)) / 2) -
                                                 (abs(v(i, j - 1, grid)) * (T(i, j - 1, Told) - T(i, j, Told)) / 2));
    return Firstsummand + SecondSummand;
}

double Tbyx2(int i, int j, Grid &grid, double dx, matrix<double> Told) {
    return (1 / (dx * dx)) * ((T(i + 1, j, Told) - 2 * T(i, j, Told) + T(i - 1, j, Told)));
}

double Tbyy2(int i, int j, Grid &grid, double dy, matrix<double> Told) {
    return (1 / (dy * dy)) * ((T(i, j + 1, Told) - 2 * T(i, j, Told) + T(i, j - 1, Told)));
}

void set_FG_boundaries(std::list<Cell *> &slipCells, std::list<Cell *> &inflowCells, std::list<Cell *> &outflowCells,
                       matrix<double> &F, matrix<double> &G, Grid &grid) {
    for (Cell *cell : slipCells) {
        int i = cell->I;
        int j = cell->J;
        if (cell->getEast()->getCellType() == cell_type::FLUID) {
            // West is not Fluid
            if (cell->getNorth()->getCellType() == cell_type::FLUID) {
                // Case B_NE
                F.at(i).at(j) = u(i, j, grid);
                G.at(i).at(j) = v(i, j, grid);
            } else if (cell->getSouth()->getCellType() == cell_type::FLUID) {
                // Case B_SE
                F.at(i).at(j) = u(i, j, grid);
                G.at(i).at(j - 1) = v(i, j - 1, grid);
            } else {
                // Case E
                F.at(i).at(j) = u(i, j, grid);
            }
        } else if (cell->getWest()->getCellType() == cell_type::FLUID) {
            // East is not Fluid
            if (cell->getNorth()->getCellType() == cell_type::FLUID) {
                // Case B_NW
                F.at(i).at(j - 1) = u(i, j - 1, grid);
                G.at(i).at(j) = v(i, j, grid);
            } else if (cell->getSouth()->getCellType() == cell_type::FLUID) {
                // Case B_SW
                F.at(i - 1).at(j) = u(i - 1, j, grid);
                G.at(i).at(j - 1) = v(i, j - 1, grid);
            } else {
                // Case W
                F.at(i - 1).at(j) = u(i - 1, j, grid);
            }
        } else if (cell->getNorth()->getCellType() == cell_type::FLUID) {
            // Case N
            G.at(i).at(j) = v(i, j, grid);
        } else if (cell->getSouth()->getCellType() == cell_type::FLUID) {
            // Case S
            G.at(i).at(j - 1) = v(i, j, grid);

        } else {
            // No Fluid boundaries
        }
    }
    for (Cell *cell : inflowCells) {
        int i = cell->I;
        int j = cell->J;
        F.at(i).at(j) = u(i, j, grid);
    }
    for (Cell *cell : outflowCells) {
        int i = cell->I;
        int j = cell->J;
        F.at(i - 1).at(j) = u(i - 1, j, grid);
    }
}

void set_T_boundaries(std::list<Cell *> &slipCells, std::list<Cell *> &inflowCells, std::list<Cell *> &outflowCells,
                      matrix<double> &Tnext, Grid &grid) {
    // We can overwrite theBC for heated walls here as the calculations
    // for the new Temperature in fluid cells has already been done and we
    // will reset the boundary conditions for the next iteration anyway

    for (Cell *cell : slipCells) {
        int i = cell->I;
        int j = cell->J;
        if (cell->getEast()->getCellType() == cell_type::FLUID) {
            // West is not Fluid
            if (cell->getNorth()->getCellType() == cell_type::FLUID) {
                // Case B_NE
                Tnext.at(i).at(j) = (Tnext.at(i + 1).at(j) + Tnext.at(i).at(j + 1)) / 2;
            } else if (cell->getSouth()->getCellType() == cell_type::FLUID) {
                // Case B_SE
                Tnext.at(i).at(j) = (Tnext.at(i + 1).at(j) + Tnext.at(i).at(j - 1)) / 2;
            } else {
                // Case E
                Tnext.at(i).at(j) = Tnext.at(i + 1).at(j);
            }
        } else if (cell->getWest()->getCellType() == cell_type::FLUID) {
            // East is not Fluid
            if (cell->getNorth()->getCellType() == cell_type::FLUID) {
                // Case B_NW
                Tnext.at(i).at(j) = (Tnext.at(i - 1).at(j) + Tnext.at(i).at(j + 1)) / 2;
            } else if (cell->getSouth()->getCellType() == cell_type::FLUID) {
                // Case B_SW
                Tnext.at(i).at(j) = (Tnext.at(i - 1).at(j) + Tnext.at(i).at(j - 1)) / 2;

            } else {
                // Case W
                Tnext.at(i).at(j) = Tnext.at(i - 1).at(j);
            }
        } else if (cell->getNorth()->getCellType() == cell_type::FLUID) {
            // Case N
            Tnext.at(i).at(j) = Tnext.at(i).at(j + 1);
        } else if (cell->getSouth()->getCellType() == cell_type::FLUID) {
            // Case S
            Tnext.at(i).at(j) = Tnext.at(i).at(j - 1);
        } else {
            // No Fluid boundaries
        }
    }

    // similar assumption as for pressure, i.e. inflow is on the left wall and outflow is on the right
    // anyway this is just a technicality as we dont have inflow or outflow cells in the worksheet examples
    for (Cell *cell : inflowCells) {
        int i = cell->I;
        int j = cell->J;
        Tnext.at(i).at(j) = Tnext.at(i + 1).at(j);
    }
    for (Cell *cell : outflowCells) {
        int i = cell->I;
        int j = cell->J;
        Tnext.at(i).at(j) = Tnext.at(i - 1).at(j);
    }
}


void exchangeMaxima(Grid &Subdomain, double &outU, double &outV) {
    double MaxU = Subdomain.getMax(velocity_type::U);
    double MaxV = Subdomain.getMax(velocity_type::V);

    double GlobalMaxU;
    double GlobalMaxV;

    MPI_Reduce(&MaxU, &GlobalMaxU, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
    MPI_Reduce(&MaxV, &GlobalMaxV, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

    MPI_Bcast(&GlobalMaxU, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&GlobalMaxV, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    outU = GlobalMaxU;
    outV = GlobalMaxV;
}


// Determines the value of F and G
void calculate_fg(
        double Re,
        double GX,
        double GY,
        double alpha,
        double dt,
        double dx,
        double dy,
        int imax,
        int jmax,
        Grid &grid,
        matrix<double> &F,
        matrix<double> &G,
        std::list<Cell *> &slipCells,
        std::list<Cell *> &inflowCells,
        std::list<Cell *> &outflowCells,
        double beta,
        int rank) {

    F.resize(grid.imaxb(), std::vector<double>(grid.jmaxb(), 0));
    G.resize(grid.imaxb(), std::vector<double>(grid.jmaxb(), 0));
    matrix<double> Tnplus1;
    grid.temperature(Tnplus1);

    // calculate values in the inner cells
    int bottomleftI = (grid.imaxb() - grid.imax()) / 2;
    int bottomleftJ = (grid.jmaxb() - grid.jmax()) / 2;
    for (int i = bottomleftI; i <= imax; i++) {
        for (int j = bottomleftJ; j <= jmax; j++) {
            if (grid.cell(i, j).getCellType() == cell_type::FLUID &&
                grid.cell(i + 1, j).getCellType() == cell_type::FLUID) {
                if (rank == 3 && i == 2 && (j == 25 || j == 26)) {

                    /*
                    std::cout << "positive: " << ubyx2(i, j, grid, dx) << ", " <<ubyy2(i, j, grid, dy)<< "\n" ;
                    std::cout << "negative: " <<u2byx(i, j, grid, dx, alpha) << ", " <<uvbyy(i, j, grid, dy, alpha)<< "\n" ;
                     */
                }


                F.at(i).at(j) = u(i, j, grid) + dt * ((1 / Re) * (ubyx2(i, j, grid, dx) + ubyy2(i, j, grid, dy)) -
                                                      u2byx(i, j, grid, dx, alpha) - uvbyy(i, j, grid, dy, alpha) + GX);
                F.at(i).at(j) -= (beta * dt / 2) * (Tnplus1.at(i).at(j) + Tnplus1.at(i + 1).at(j)) * GX;
            }
        }
    }

    for (int i = bottomleftI; i <= imax; i++) {
        for (int j = bottomleftJ; j <= jmax; j++) {
            if (grid.cell(i, j).getCellType() == cell_type::FLUID &&
                grid.cell(i, j + 1).getCellType() == cell_type::FLUID) {
                G.at(i).at(j) = v(i, j, grid) + dt * ((1 / Re) * (vbyx2(i, j, grid, dx) + vbyy2(i, j, grid, dy)) -
                                                      uvbyx(i, j, grid, dx, alpha) - v2byy(i, j, grid, dy, alpha) + GY);
                G.at(i).at(j) -= (beta * dt / 2) * (Tnplus1.at(i).at(j) + Tnplus1.at(i).at(j + 1)) * GY;

            }
        }
    }

    // set values at the boundary
    set_FG_boundaries(slipCells, inflowCells, outflowCells, F, G, grid);
    if (rank == 3) {
        std::cout << "F: " << F.at(25).at(26) << ", " << F.at(26).at(26) << "\n ";
    }

}

// This operation computes the right hand side of the pressure poisson equation.
void calculate_rs(
        double dt,
        double dx,
        double dy,
        int imax,
        int jmax,
        matrix<double> &F,
        matrix<double> &G,
        matrix<double> &RS,
        Grid &grid, int rank) {
    // this is because we don't have imaxb and jmaxb or the grid here
    const matrix<double> &F_ref = F;
    RS = F_ref;

    int bottomleftI = (grid.imaxb() - grid.imax()) / 2;
    int bottomleftJ = (grid.jmaxb() - grid.jmax()) / 2;

    for (int i = bottomleftI; i <= imax; i++) {
        for (int j = bottomleftJ; j <= jmax; j++) {
            RS.at(i).at(j) = (1 / dt) * ((1 / dx) * (F.at(i).at(j) - F.at(i - 1).at(j)) +
                                         (1 / dy) * (G.at(i).at(j) - G.at(i).at(j - 1)));
            if (rank == 3 && i == 26 && (j == 26)) {
                std::cout << "RS: " << RS.at(i).at(j) << "\n ";
            }
        }
    }
}

// Determines the maximal time step size
void calculate_dt(double Re, double tau, double *dt, double dx, double dy, double PR,
                  double maxU, double maxV) {
    double Tdiffusivity = 1 / (PR * Re); // since we assume RE = 1/nu
    *dt = tau * std::min(std::min(dx / maxU, dy / maxV), std::min(
            (Re / 2) * (1 / (1 / (dx * dx) + 1 / (dy * dy))),
            (1 / (2 * Tdiffusivity)) * (1 / (1 / (dx * dx) + 1 / (dy * dy))))); // 1/0 is inf, min can handle inf
}

void calculate_uv(
        double dt,
        double dx,
        double dy,
        int imax,
        int jmax,
        Grid &grid,
        matrix<double> &F,
        matrix<double> &G,
        int rank) {

    matrix<double> P;
    grid.pressure(P);

    int bottomleftI = (grid.imaxb() - grid.imax()) / 2;
    int bottomleftJ = (grid.jmaxb() - grid.jmax()) / 2;

    // calculating and writing u
    for (int i = bottomleftI; i <= imax; i++) {
        for (int j = bottomleftJ; j <= jmax; j++) {
            if (grid.cell(i, j).getCellType() == cell_type::FLUID &&
                grid.cell(i + 1, j).getCellType() == cell_type::FLUID) {
                if (rank == 3 && i == 2 && (j == 25 || j == 26)) {
                    /*std::cout << "positive: " << F.at(i).at(j) << "\n" ;
                    std::cout << "negative: " <<(P.at(i + 1).at(j) - P.at(i).at(j))<< "\n" ;*/
                }
                if (rank == 3 && i == 25 && (j == 26)) {
                    std::cout << "Pressure Grad = " << (P.at(i + 1).at(j) - P.at(i).at(j)) << "\n";
                }
                double vel = F.at(i).at(j) - (dt / dx) * (P.at(i + 1).at(j) - P.at(i).at(j));
                grid.cell(i, j).set_velocity(vel, velocity_type::U);
            }
        }
    }
    if (rank == 3) {
        std::cout << "u= " << grid.cell(25, 26).velocity(velocity_type::U) << ", "
                  << grid.cell(26, 26).velocity(velocity_type::U) << "\n";
    }

    for (int i = bottomleftI; i <= imax; i++) {
        for (int j = bottomleftJ; j <= jmax; j++) {
            if (grid.cell(i, j).getCellType() == cell_type::FLUID &&
                grid.cell(i, j + 1).getCellType() == cell_type::FLUID) {
                double vel = G.at(i).at(j) - (dt / dy) * (P.at(i).at(j + 1) - P.at(i).at(j));
                grid.cell(i, j).set_velocity(vel, velocity_type::V);
            }
        }
    }

}

void
calculate_T(double dt, double dx, double dy, int imax, int jmax, Grid &grid, double gammafactor, double PR, double Re,
            std::list<Cell *> &slipCells,
            std::list<Cell *> &inflowCells,
            std::list<Cell *> &outflowCells) {
    matrix<double> Told;
    grid.temperature(Told);
    matrix<double> Tnext;
    Tnext.resize(imax + 2, std::vector<double>(jmax + 2, 0));
    matrix<double> Tbydt;
    Tbydt.resize(imax + 2, std::vector<double>(jmax + 2, 0));
    double Tdiffusivity = 1 / (PR * Re); // since we assume RE = 1/nu

    int bottomleftI = (grid.imaxb() - grid.imax()) / 2;
    int bottomleftJ = (grid.jmaxb() - grid.jmax()) / 2;

    for (int i = bottomleftI; i <= imax; i++) {
        for (int j = bottomleftJ; j <= jmax; j++) {
            if (grid.cell(i, j).getCellType() == cell_type::FLUID) {
                // Equation 34 rearnged
                Tbydt.at(i).at(j) = Tdiffusivity * (Tbyx2(i, j, grid, dx, Told) + Tbyy2(i, j, grid, dy, Told)) -
                                    uTbyx(i, j, grid, dx, gammafactor, Told) - vTbyy(i, j, grid, dy, gammafactor, Told);
            }
        }
    }
    for (int i = bottomleftI; i <= imax; i++) {
        for (int j = bottomleftJ; j <= jmax; j++) {
            if (grid.cell(i, j).getCellType() == cell_type::FLUID) {
                Tnext.at(i).at(j) = Told.at(i).at(j) + dt * Tbydt.at(i).at(j);
            }
        }
    }
    set_T_boundaries(slipCells, inflowCells, outflowCells, Tnext, grid);
    grid.set_temperature(Tnext);
}

void setDoublePointerToZero(double **P, int rows, int columns) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            P[i][j] = 0;
        }
    }
}

double ldcp_u2byx(double **u, int i, int j, double dx, double gammaFactor) {
    double uij = u[i][j];
    double uiplus1j = u[i + 1][j];
    double uiminus1j = u[i - 1][j];
    double firstSummand = (1 / dx) * (pow((uij + uiplus1j) / 2, 2) -
                                      pow((uiminus1j + uij) / 2, 2));
    double scndSummand = gammaFactor * (1 / dx) *
                         ((abs(uij + uiplus1j) / 2) * ((uij - uiplus1j) / 2)
                          - (abs(uiminus1j + uij) / 2) * ((uiminus1j - uij) / 2)); // may be fabs() instead of abs()?
    return firstSummand + scndSummand;
}

double ldcp_uvbyy(double **u, double **v, int i, int j, double dy, double gammaFactor) {
    double uij = u[i][j];
    double uijplus1 = u[i][j + 1];
    double uijminus1 = u[i][j - 1];
    double vij = v[i - 1][j + 1];
    double viplus1j = v[i][j + 1];
    double viplus1jminus1 = v[i][j];
    double vijminus1 = v[i - 1][j];

    double firstSummandPreMinus = ((vij + viplus1j) / 2) * ((uij + uijplus1) / 2);
    double firstSummandPostMinus =
            ((vijminus1 + viplus1jminus1) / 2) * ((uijminus1 + uij) / 2);
    double firstSummand = (1 / dy) * (firstSummandPreMinus - firstSummandPostMinus);
    double scndSummandPreMinus =
            (abs(vij + viplus1j) / 2) * ((uij - uijplus1) / 2);
    double scndSummandPostMinus =
            (abs(vijminus1 + viplus1jminus1) / 2) * ((uijminus1 - uij) / 2);
    double scndSummand = gammaFactor * (1 / dy) * (scndSummandPreMinus + scndSummandPostMinus);
    return firstSummand + scndSummand;
}

double ldcp_ubyx2(double **u, int i, int j, double dx) {
    return (u[i + 1][j] - 2 * u[i][j] + u[i - 1][j]) / (dx * dx);
}

double ldcp_ubyy2(double **u, int i, int j, double dy) {
    return (u[i][j + 1] - 2 * u[i][j] + u[i][j - 1]) / (dy * dy);
}

double ldcp_pbyx(double **p, int i, int j) {
    return (p[i + 1][j] - p[i][j]) / 2;
}

// all operators from Eq (5)
double ldcp_v2byy(double **v, int i, int j, double dy, double gammaFactor) {
    double vij = v[i][j];
    double vijplus1 = v[i][j + 1];
    double vijminus1 = v[i][j - 1];

    double firstSummand = (1 / dy) * (pow((vij + vijplus1) / 2, 2) -
                                      pow((vijminus1 + vij) / 2, 2));
    double scndSummand = gammaFactor * (1 / dy) *
                         ((abs(vij + vijplus1) / 2) * ((vij - vijplus1) / 2)
                          - (abs(vijminus1 + vij) / 2) * ((vijminus1 - vij) / 2));
    return firstSummand + scndSummand;
}

double ldcp_uvbyx(double **u, double **v, int i, int j, double dx, double gammaFactor) {
    double uij = u[i + 1][j - 1];
    double uiminus1jplus1 = u[i][j];
    double uiminus1j = u[i][j - 1];
    double uijplus1 = u[i + 1][j];
    double vij = v[i][j];
    double viplus1j = v[i + 1][j];
    double viminus1j = v[i - 1][j];

    double firstSummandPreMinus = ((uij + uijplus1) / 2) * ((vij + viplus1j) / 2);
    double firstSummandPostMinus =
            ((uiminus1j + uiminus1jplus1) / 2) * ((viminus1j + vij) / 2);
    double firstSummand = (1 / dx) * (firstSummandPreMinus - firstSummandPostMinus);
    double scndSummandPreMinus =
            (abs(uij + uijplus1) / 2) * ((vij - viplus1j) / 2);
    double scndSummandPostMinus =
            (abs(uiminus1j + uiminus1jplus1) / 2) * ((viminus1j - vij) / 2);
    double scndSummand = gammaFactor * (1 / dx) * (scndSummandPreMinus + scndSummandPostMinus);
    return firstSummand + scndSummand;
}

double ldcp_vbyx2(double **v, int i, int j, double dx) {
    return (v[i + 1][j] - 2 * v[i][j] + v[i - 1][j]) / (dx * dx);
}

double ldcp_vbyy2(double **v, int i, int j, double dy) {
    return (v[i][j + 1] - 2 * v[i][j] + v[i][j - 1]) / (dy * dy);
}

double ldcp_pbyy(double **p, int i, int j) {
    return (p[i][j + 1] - p[i][j]) / 2;
}


// all operators from Eq (6)
double ldcp_ubyx(double **u, int i, int j) {
    return (u[i][j] - u[i - 1][j]) / 2;
}

double ldcp_vbyy(double **v, int i, int j) {
    return (v[i][j] - v[i][j - 1]) / 2;
}


void
BoundaryLidDrivenCavityParallel(double **F, double **G, double **U, double **V, int imax, int jmax, int whichboundary) {
    // 0 = left boundary
    // 1 = right boundary
    // 2 = bottom boundary
    // 3 = top boundary
    assert(0 <= whichboundary && 3 >= whichboundary);


    switch (whichboundary) {
        case 0:
            for (int j = 0; j < jmax; j++) {
                F[1][j + 1] = U[1][j + 1];
            }
            break;
        case 1:
            for (int j = 0; j < jmax; j++) {
                F[imax + 1][j + 1] = U[imax + 1][j + 1];
            }
            break;
        case 2:
            for (int i = 0; i < imax; i++) {
                G[i + 1][1] = V[i + 1][1];
            }
            break;
        case 3:
            for (int i = 0; i < imax; i++) {
                G[i + 1][jmax + 1] = V[i + 1][jmax + 1];
            }
            break;

    }

}


// Determines the value of F and G
void ldcp_calculate_fg(
        double Re,
        double GX,
        double GY,
        double alpha,
        double dt,
        double dx,
        double dy,
        int imax,
        int jmax,
        double **u,
        double **v,
        double **F,
        double **G,
        bool leftBoundary,
        bool rightBoundary,
        bool topBoundary,
        bool bottomBoundary,int rank) {

    setDoublePointerToZero(F, imax + 3, jmax + 2);
    setDoublePointerToZero(G, imax + 2, jmax + 3);

    // calculate values in the inner cells
    for (int i = 1; i <= imax + 1; i++) {
        for (int j = 1; j <= jmax; j++) {
            F[i][j] = u[i][j] + dt * ((1 / Re) * (ldcp_ubyx2(u, i, j, dx) + ldcp_ubyy2(u, i, j, dy)) -
                                      ldcp_u2byx(u, i, j, dx, alpha) - ldcp_uvbyy(u, v, i, j, dy, alpha) + GX);

        }
    }


    for (int i = 1; i <= imax; i++) {
        for (int j = 1; j <= jmax + 1; j++) {
            G[i][j] = v[i][j] + dt * ((1 / Re) * (ldcp_vbyx2(v, i, j, dx) + ldcp_vbyy2(v, i, j, dy)) -
                                      ldcp_uvbyx(u, v, i, j, dx, alpha) - ldcp_v2byy(v, i, j, dy, alpha) + GY);
        }
    }
    //if (rank == 3) {
    //    std::cout << F[25][25] << ", "<< F[26][25] << "\n";
    //}

    if (leftBoundary) {
        BoundaryLidDrivenCavityParallel(F, G, u, v, imax, jmax, 0);
    }
    if (rightBoundary) {
        BoundaryLidDrivenCavityParallel(F, G, u, v, imax, jmax, 1);
    }
    if (bottomBoundary) {
        BoundaryLidDrivenCavityParallel(F, G, u, v, imax, jmax, 2);
    }
    if (topBoundary) {
        BoundaryLidDrivenCavityParallel(F, G, u, v, imax, jmax, 3);
    }




}

void ldcp_calculate_rs(
        double dt,
        double dx,
        double dy,
        int imax,
        int jmax,
        double **F,
        double **G,
        double **RS,
        int rank) {

    setDoublePointerToZero(RS, imax, jmax);

    for (int i = 0; i < imax; i++) {
        for (int j = 0; j < jmax; j++) {
            RS[i][j] = (1 / dt) * ((1 / dx) * (F[i + 2][j + 1] - F[i + 1][j + 1]) +
                                   (1 / dy) * (G[i + 1][j + 2] - G[i + 1][j + 1]));

        }
    }
}

void ldcp_calculate_uv(
        double dt,
        double dx,
        double dy,
        int imax,
        int jmax,
        double **F,
        double **G,
        double **U,
        double **V,
        double **P) {

    // calculating and writing u and v
    for (int i = 1; i <= imax + 1; i++) {
        for (int j = 1; j <= jmax; j++) {
            U[i][j] = F[i][j] - (dt / dx) * (P[i][j] - P[i - 1][j]);
        }
    }

    for (int i = 1; i <= imax; i++) {
        for (int j = 1; j <= jmax + 1; j++) {
            V[i][j] = G[i][j] - (dt / dy) * (P[i][j] - P[i][j - 1]);
        }
    }
}



