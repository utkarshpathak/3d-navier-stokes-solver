#include "sor.hpp"
#include <cmath>
#include <assert.h>
#include "list"
#include "mpi.h"
#include "parallel.h"
#include "algorithm"

double **toPointer_(matrix<double> &M, int a, int b) {
    double **out;
    out = new double *[a];
    for (int i = 0; i < a; i++) {
        out[i] = new double[b];
        for (int j = 0; j < b; j++) {
            out[i][j] = M.at(i).at(j);
        }
    }
    return out;
}

void toMatrix_(double **M, matrix<double> &P, int a, int b) {
    P.resize(a, std::vector(b, 0.0));
    for (int i = 0; i < a; i++) {
        for (int j = 0; j < b; j++) {
            P[i][j] = M[i][j];
        }
    }

}


void set_boundary(matrix<double> &P, std::list<Cell *> &slipCells, std::list<Cell *> &inFlowCells,
                  std::list<Cell *> &outFlowCells) {
    for (Cell *cell: slipCells) {
        assert(cell->getCellType() == cell_type::NOSLIP || cell->getCellType() == cell_type::FREESLIP);
        int i = cell->I;
        int j = cell->J;
        if (cell->getEast()->getCellType() == cell_type::FLUID) {
            // West is not Fluid
            if (cell->getNorth()->getCellType() == cell_type::FLUID) {
                // Case B_NE
                P.at(i).at(j) = ((P.at(i + 1).at(j) + P.at(i).at(j + 1)) / 2);
            } else if (cell->getSouth()->getCellType() == cell_type::FLUID) {
                // Case B_SE
                P.at(i).at(j) = ((P.at(i + 1).at(j) + P.at(i).at(j - 1)) / 2);
            } else {
                // Case E
                P.at(i).at(j) = P.at(i + 1).at(j);
            }
        } else if (cell->getWest()->getCellType() == cell_type::FLUID) {
            // East is not Fluid
            if (cell->getNorth()->getCellType() == cell_type::FLUID) {
                // Case B_NW
                P.at(i).at(j) = ((P.at(i - 1).at(j) + P.at(i).at(j + 1)) / 2);
            } else if (cell->getSouth()->getCellType() == cell_type::FLUID) {
                // Case B_SW
                P.at(i).at(j) = ((P.at(i - 1).at(j) + P.at(i).at(j - 1)) / 2);
            } else {
                // Case W
                P.at(i).at(j) = P.at(i - 1).at(j);
            }
        } else if (cell->getNorth()->getCellType() == cell_type::FLUID) {
            // Case N
            P.at(i).at(j) = P.at(i).at(j + 1);
        } else if (cell->getSouth()->getCellType() == cell_type::FLUID) {
            // Case S
            P.at(i).at(j) = P.at(i).at(j - 1);

        } else {
            // No Fluid boundaries
        }
    }
    for (Cell *cell: inFlowCells) {
        int i = cell->I;
        int j = cell->J;

        // assumtion: inflow Cells are always on the Western border.
        // Generalisation will come later on but for now this seems reasonable due to the three example problems
        P.at(i).at(j) = P.at(i + 1).at(j);
    }
    for (Cell *cell: outFlowCells) {
        int i = cell->I;
        int j = cell->J;

        // similar assumption for outflow cells right now
        P.at(i).at(j) = P.at(i - 1).at(j);
    }
}

void sor(
        double omg,
        double dx,
        double dy,
        int imax,
        int jmax,
        Grid &grid,
        matrix<double> &RS,
        double *res,
        std::list<Cell *> &slipCells,
        std::list<Cell *> &inflowCells,
        std::list<Cell *> &outflowCells,
        std::list<Cell *> &fluidCells
) {
    int i, j;
    double rloc;

    int bottomleftI = (grid.imaxb() - grid.imax()) / 2;
    int bottomleftJ = (grid.jmaxb() - grid.jmax()) / 2;

    double dx2_inv = 1 / (dx * dx);
    double dy2_inv = 1 / (dy * dy);

    double coeff = omg / (2.0 * (dx2_inv + dy2_inv));
    matrix<double> P;
    grid.pressure(P);

    set_boundary(P, slipCells, inflowCells, outflowCells);

    /* SOR iteration */
    for (i = bottomleftI; i <= imax; i++) {
        for (j = bottomleftJ; j <= jmax; j++) {
            if (grid.cell(i, j).getCellType() == cell_type::FLUID) {
                P.at(i).at(j) = (1.0 - omg) * P.at(i).at(j)
                                + coeff * ((P.at(i + 1).at(j) + P.at(i - 1).at(j)) * dx2_inv +
                                           (P.at(i).at(j + 1) + P.at(i).at(j - 1)) * dy2_inv - RS.at(i).at(j));
            }
        }
    }

    /* compute the residual */
    rloc = 0;
    int sanity = 0;
    for (i = bottomleftI; i <= imax; i++) {
        for (j = bottomleftJ; j <= jmax; j++) {
            // We updated this part as it accelerated the execution by around 100%.
            // We hope it is fine that readability suffered a little bit here but
            // as this is not part of the "to implement" code we thought you dont
            // have to evaluate it either, so it can be messy
            if (grid.cell(i, j).getCellType() == cell_type::FLUID) {
                sanity += 1;
                double pij = P[i][j];
                double rsij = RS[i][j];
                double pi_1j = P[i - 1][j];
                double pi1j = P[i + 1][j];
                double pij_1 = P[i][j - 1];
                double pij1 = P[i][j + 1];

                rloc += ((pi1j - 2.0 * pij + pi_1j) * dx2_inv +
                         (pij1 - 2.0 * pij + pij_1) * dy2_inv - rsij) *
                        ((pi1j - 2.0 * pij + pi_1j) * dx2_inv +
                         (pij1 - 2.0 * pij + pij_1) * dy2_inv - rsij);
            }
        }
    }
    assert(sanity == fluidCells.size());
    rloc = rloc / fluidCells.size();
    rloc = sqrt(rloc);
    /* set residual */
    *res = rloc;

    /* set boundary values */
    set_boundary(P, slipCells, inflowCells, outflowCells);

    grid.set_pressure(P);

}

void BoundaryLidDrivenCavityParallel(double **P, int imax, int jmax, int whichboundary) {
    // 0 = left boundary
    // 1 = right boundary
    // 2 = bottom boundary
    // 3 = top boundary
    assert(0 <= whichboundary && 3 >= whichboundary);


    switch (whichboundary) {
        case 0:
            for (int j = 1; j <= jmax; j++) {
                P[0][j] = P[1][j];
            }
            break;
        case 1:
            for (int j = 1; j <= jmax; j++) {
                P[imax + 1][j] = P[imax][j];
            }
            break;
        case 2:
            for (int i = 1; i <= imax; i++) {
                P[i][0] = P[i][1];
            }
            break;
        case 3:
            for (int i = 1; i <= imax; i++) {
                P[i][jmax + 1] = P[i][jmax];
            }
            break;

    }

}


void sorParallel(
        double omg,
        double dx,
        double dy,
        int imax,
        int jmax,
        double **RS,
        double **P,
        double *res,
        bool leftBoundary,
        bool rightBoundary,
        bool topBoundary,
        bool bottomBoundary,
        int il, int ir, int jb, int jt, int rank_l, int rank_r, //TODO: Remove parameters that are not needed
        int rank_b, int rank_t, int rank) {
    int i, j;
    double rloc;

    double dx2_inv = 1 / (dx * dx);
    double dy2_inv = 1 / (dy * dy);
    double coeff = omg / (2.0 * (dx2_inv + dy2_inv));

    if (leftBoundary) {
        BoundaryLidDrivenCavityParallel(P, imax, jmax, 0);
    }
    if (rightBoundary) {
        BoundaryLidDrivenCavityParallel(P, imax, jmax, 1);
    }
    if (bottomBoundary) {
        BoundaryLidDrivenCavityParallel(P, imax, jmax, 2);
    }
    if (topBoundary) {
        BoundaryLidDrivenCavityParallel(P, imax, jmax, 3);
    }

    /*if (rank == 0) {
        for (int j = 26; j <= 50; j++) {
            std::cout << "before" << P[26][j] << "\n";
        }
    }
    if (rank == 2) {
        for (int j = 1; j <= jmax; j++) {
            std::cout << "before" << P[26][j] << "\n";
        }
    }*/

    /* SOR iteration */


    for (int i = 1; i <= imax; i++) {
        for (int j = 1; j <= jmax; j++) {
            P[i][j] = (1.0 - omg) * P[i][j] + coeff * ((P[i + 1][j] + P[i - 1][j]) * dx2_inv +
                                                       (P[i][j + 1] + P[i][j - 1]) * dy2_inv - RS[i - 1][j - 1]);
        }
    }


    pressure_comm(P, il, ir, jb, jt, rank_l, rank_r, rank_b, rank_t, MPI_STATUS_IGNORE, rank);


    /* compute the residual */
    rloc = 0;

    for (i = 1; i <= imax; i++) {
        for (j = 1; j <= jmax; j++) {
            double pij = P[i][j];
            double rsij = RS[i - 1][j - 1];
            double pi_1j = P[i - 1][j];
            double pi1j = P[i + 1][j];
            double pij_1 = P[i][j - 1];
            double pij1 = P[i][j + 1];

            rloc += ((pi1j - 2.0 * pij + pi_1j) * dx2_inv +
                     (pij1 - 2.0 * pij + pij_1) * dy2_inv - rsij) *
                    ((pi1j - 2.0 * pij + pi_1j) * dx2_inv +
                     (pij1 - 2.0 * pij + pij_1) * dy2_inv - rsij);
        }
    }

    rloc = rloc / (imax * jmax);
    rloc = sqrt(rloc);

    /* set residual */
    *res = rloc;
    //std::cout<< "rank: " << rank << " " << leftBoundary << ", " << rightBoundary << ", " << bottomBoundary<< ", " << topBoundary  << "\n";
    if (leftBoundary) {
        BoundaryLidDrivenCavityParallel(P, imax, jmax, 0);
    }
    if (rightBoundary) {
        BoundaryLidDrivenCavityParallel(P, imax, jmax, 1);
    }
    if (bottomBoundary) {
        BoundaryLidDrivenCavityParallel(P, imax, jmax, 2);
    }
    if (topBoundary) {
        BoundaryLidDrivenCavityParallel(P, imax, jmax, 3);
    }
/*
    if (rank == 2) {
        for (int j = 1; j <= jmax; j++) {
            std::cout << "after"  << P[26][j] << "\n";
        }
    }

    if (rank == 0) {
        for (int j = 26; j <= 50; j++) {
            std::cout << "after" << P[26][j] << "\n";
        }
    }*/




}
