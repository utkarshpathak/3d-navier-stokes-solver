//
// Created by Moritz Gnisia on 05.04.20.
//

#ifndef CFDLAB_ENUMS_H
#define CFDLAB_ENUMS_H

enum class velocity_type {
    U,
    V
};

enum class border_position {
    TOP,
    BOTTOM,
    LEFT,
    RIGHT
};

enum class matrix_selection {
    ROW,
    COLUMN
};

enum class read_type {
    INT,
    DOUBLE
};

// defines the type of a cell. Test for boundary is: c != cell_type::FLUID
// also uses same order as the example channel, i.e. 0 = noSlip, 4 = Fluid,...
enum class cell_type {
    NOSLIP,
    FREESLIP,
    OUTFLOW,
    INFLOW,
    FLUID
};

#endif //CFDLAB_ENUMS_H
