#ifndef __SOR_H_
#define __SOR_H_

#include "list"
#include "datastructures.hpp"
#include "grid.hpp"

/**
 * One GS iteration for the pressure Poisson equation. Besides, the routine must 
 * also set the boundary values for P according to the specification. The 
 * residual for the termination criteria has to be stored in res.
 * 
 * An \omega = 1 GS - implementation is given within sor.c.
 */
/*void sor(
        double omg,
        double dx,
        double dy,
        int    imax,
        int    jmax,
        matrix &P,
        matrix &RS,
        double *res
);*/


void sor(
        double omg,
        double dx,
        double dy,
        int imax,
        int jmax,
        Grid &grid,
        matrix<double> &RS,
        double *res,
        std::list<Cell*> &boundary,
        std::list<Cell*> &inflow,
        std::list<Cell*> &outflow,
        std::list<Cell*> &fluid

);

void sorParallel(
        double omg,
        double dx,
        double dy,
        int imax,
        int jmax,
        double **RS,
        double **P,
        double *res,
        bool leftBoundary,
        bool rightBoundary,
        bool topBoundary,
        bool bottomBoundary,
        int il, int ir, int jb, int jt, int rank_l, int rank_r, //TODO: Remove parameters that are not needed
        int rank_b, int rank_t, int rank);


#endif
