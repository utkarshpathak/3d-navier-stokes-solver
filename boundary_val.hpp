#ifndef __RANDWERTE_HPP__
#define __RANDWERTE_HPP__

#include "list"
#include "cstring"
#include "helper.hpp"
#include "datastructures.hpp"
#include "grid.hpp"


/**
 * The boundary values of the problem are set.
 */
void PlaneShearFlow(int imax, int jmax, Grid &grid, std::list<Cell *> &cells, double inflowU, double inflowV);

void KarmanVortexStreet(int imax, int jmax, Grid &grid, std::list<Cell *> &cells, double inflowU, double inflowV);

void FlowOverStep(int imax, int jmax, Grid &grid, std::list<Cell *> &cells, double inflowU, double inflowV);

void NaturalConvection(int imax, int jmax, Grid &grid, double T_h, double T_c, std::list<Cell *> slipcells);

void FluidTrap(int imax, int jmax, Grid &grid, double T_h, double T_c, std::list<Cell *> slipcells);

void boundaryvalues(int imax, int jmax, Grid &grid);

void LidDrivenCavity(int imax, int jmax, Grid &grid, double inflowU, double inflowV);

void LidDrivenCavityParallel(int rank, int iproc, int jproc, int imax, int jmax, Grid &grid, double inflowU, double inflowV);

#endif
