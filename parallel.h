//
// Created by max on 02.06.20.
//

#include <mpi.h>
#include "datastructures.hpp"

#ifndef CFDLAB_PARALLEL_H
#define CFDLAB_PARALLEL_H

void init_parallel(int iproc,int jproc,int imax,int jmax,
                   int myrank,int *il,int *ir,int *jb,int *jt,
                   int *rank_l,int *rank_r,int *rank_b,int *rank_t, int *omg_i,int *omg_j,int num_proc);

void pressure_comm(double **P,int il,int ir,int jb,int jt,
                   int rank_l,int rank_r,int rank_b,int rank_t, MPI_Status *status, int rank);

void uv_comm(double **U,double** V, int il, int ir, int jb, int jt, int rank_l, int rank_r, int rank_b, int rank_t,
             MPI_Status *status, int rank);

#endif //CFDLAB_PARALLEL_H
;